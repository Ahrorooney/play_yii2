<?php //$user = \common\models\users\User::findOne(Yii::$app->user->id); ?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
        <img src="<?=$assetDir?>/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Senat Admin</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
<!--                <a href="#" class="d-block">--><?php //echo $user->username; ?><!--</a>-->
                <a href="#" class="d-block">Admin</a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
        <!-- href be escaped -->
        <!-- <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div> -->

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <?php
            if (!Yii::$app->user->isGuest){
                echo \hail812\adminlte\widgets\Menu::widget([
                    'items' => [
                        [
                            'label' => 'Users',
                            'icon' => 'users',
                            'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'users',
                            'url' => ['/user']
                        ],
                        [
                            'label' => 'Authorities',
                            'icon' => 'user-friends',
                            'active' => (Yii::$app->session->get('active_menu_item_sidebar') == 'authority') ||
                                (Yii::$app->session->get('active_menu_item_sidebar') == 'authority_type'),
                            'items' => [
                                [
                                    'label' => 'Authority',
                                    'icon' => 'user-tie',
                                    'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'authority',
                                    'url' => ['/authority']
                                ],
                                [
                                    'label' => 'Authority Type',
                                    'icon' => 'portrait',
                                    'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'authority_type',
                                    'url' => ['/authority-type']
                                ],
                            ]
                        ],
                        [
                            'label' => 'Departments',
                            'icon' => 'id-card',
                            'active' => (Yii::$app->session->get('active_menu_item_sidebar') == 'department') ||
                                (Yii::$app->session->get('active_menu_item_sidebar') == 'department_type'),
                            'items' => [
                                [
                                    'label' => 'Department',
                                    'icon' => 'id-card',
                                    'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'department',
                                    'url' => ['/department']
                                ],
                                [
                                    'label' => 'Department Type',
                                    'icon' => 'mouse-pointer',
                                    'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'department_type',
                                    'url' => ['/department-type']
                                ],
                            ]
                        ],
                        [
                            'label' => 'Document Type',
                            'icon' => 'file-alt',
                            'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'document_type',
                            'url' => ['/document-type']
                        ],
                        [
                            'label' => 'Positions',
                            'icon' => 'user-secret',
                            'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'position',
                            'url' => ['/position']
                        ],
                        [
                            'label' => 'Helpers',
                            'icon' => 'user-secret',
                            'active' => in_array(Yii::$app->session->get('active_menu_item_sidebar'),
                                ['appeal-form','appeal-problem', 'appeal-source', 'appeal-sphere', 'appeal-type',
                                    'authority-categories', 'contract-type', 'decision-type', 'decree-sphere',
                                    'decree-type', 'document-character', 'protocol-type', 'task-status', 'task-type',
                                    'territoriality-type']),
                            'items' => [
                                [
                                    'label' => 'Appeal Form',
                                    'icon' => 'user-secret',
                                    'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'appeal-form',
                                    'url' => ['/appeal-form']
                                ],
                                [
                                    'label' => 'Appeal Problem',
                                    'icon' => 'user-secret',
                                    'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'appeal-problem',
                                    'url' => ['/appeal-problem']
                                ],
                                [
                                    'label' => 'Appeal Source',
                                    'icon' => 'user-secret',
                                    'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'appeal-source',
                                    'url' => ['/appeal-source']
                                ],
                                [
                                    'label' => 'Appeal Sphere',
                                    'icon' => 'user-secret',
                                    'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'appeal-sphere',
                                    'url' => ['/appeal-sphere']
                                ],
                                [
                                    'label' => 'Appeal Type',
                                    'icon' => 'user-secret',
                                    'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'appeal-type',
                                    'url' => ['/appeal-type']
                                ],
                                [
                                    'label' => 'Authority Categories',
                                    'icon' => 'user-secret',
                                    'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'authority-categories',
                                    'url' => ['/authority-categories']
                                ],
                                [
                                    'label' => 'Contract Type',
                                    'icon' => 'user-secret',
                                    'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'contract-type',
                                    'url' => ['/contract-type']
                                ],
                                [
                                    'label' => 'Decision Type',
                                    'icon' => 'user-secret',
                                    'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'decision-type',
                                    'url' => ['/decision-type']
                                ],
                                [
                                    'label' => 'Decree Sphere',
                                    'icon' => 'user-secret',
                                    'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'decree-sphere',
                                    'url' => ['/decree-sphere']
                                ],
                                [
                                    'label' => 'Decree Type',
                                    'icon' => 'user-secret',
                                    'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'decree-type',
                                    'url' => ['/decree-type']
                                ],
                                [
                                    'label' => 'Document Character',
                                    'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'document-character',
                                    'icon' => 'user-secret',
                                    'url' => ['/document-character']
                                ],
                                [
                                    'label' => 'Protocol Type',
                                    'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'protocol-type',
                                    'icon' => 'user-secret',
                                    'url' => ['/protocol-type']
                                ],
                                [
                                    'label' => 'Task Status',
                                    'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'task-status',
                                    'icon' => 'user-secret',
                                    'url' => ['/task-status']
                                ],
                                [
                                    'label' => 'Task Type',
                                    'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'task-type',
                                    'icon' => 'user-secret',
                                    'url' => ['/task-type']
                                ],
                                [
                                    'label' => 'Territoriality Type',
                                    'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'territoriality-type',
                                    'icon' => 'user-secret',
                                    'url' => ['/territoriality-type']
                                ],

                            ]
                        ],
                        [
                            'label' => 'Regions',
                            'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'regions',
                            'icon' => 'city',
                            'url' => ['/regions']
                        ],
                        [
                            'label' => 'Role',
                            'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'role',
                            'icon' => 'dot-circle',
                            'url' => ['/role']
                        ],
                        [
                            'label' => 'Translate',
                            'active' => Yii::$app->session->get('active_menu_item_sidebar') == 'translate',
                            'icon' => 'language',
                            'url' => ['/translate']
                        ],
                    ],
                ]);
            }
            ?>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>