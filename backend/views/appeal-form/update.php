<?php

/* @var $this yii\web\View */
/* @var $model common\models\helper\AppealForm */

$this->title = 'Update Appeal Form: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Appeal Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <?=$this->render('_form', [
                        'model' => $model
                    ]) ?>
                </div>
            </div>
        </div>
        <!--.card-body-->
    </div>
    <!--.card-->
</div>