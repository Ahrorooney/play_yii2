<?php

use common\models\users\Department;
use common\models\users\DepartmentType;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DepartmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Departments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-md-12">
                            <?= Html::a('Create Department', ['create'], ['class' => 'btn btn-success']) ?>
                        </div>
                    </div>


                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            [
                                'class' => 'yii\grid\SerialColumn',
                                'contentOptions' => ['style' => 'width:40px']
                            ],
                            [
                                'attribute' => 'id',
                                'contentOptions' => ['style' => 'width:100px']
                            ],
                            [
                                'attribute' => 'title_oz',
                                'value' => function (Department $auth) {
                                    return Html::ul([
                                        'OZ - ' . $auth->title_oz,
                                        'UZ - ' . $auth->title_uz,
                                        'RU - ' . $auth->title_ru,
                                        'QR - ' . $auth->title_qr,
                                    ]);
                                },
                                'format' => 'raw',
                                'options' => [
                                    'class' => 'col-md-4'
                                ]
                            ],
                            [
                                'label' => 'Type id',
                                'filter' => ArrayHelper::map(DepartmentType::find()->asArray()->all(), 'title_oz', 'title_oz'),
                                'attribute' => 'departmentType',
                                'value' => 'departmentType.title_oz'
                            ],
                            //'created_at',
                            //'creator_id',
                            //'updated_at',
                            //'modifier_id',

                            ['class' => 'hail812\adminlte3\yii\grid\ActionColumn'],
                        ],
                        'summaryOptions' => ['class' => 'summary mb-2'],
                        'pager' => [
                            'class' => 'yii\bootstrap4\LinkPager',
                        ]
                    ]); ?>


                </div>
                <!--.card-body-->
            </div>
            <!--.card-->
        </div>
        <!--.col-md-12-->
    </div>
    <!--.row-->
</div>
