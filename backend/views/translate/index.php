<?php

use yii\bootstrap4\Alert;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TranslateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Translates';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-md-12">
                            <?= Html::a('Create Translate', ['create'], ['class' => 'btn btn-success']) ?>
                            <?= Html::a('<i class="fas fa-file-download"></i> Translation Zip', ['translate/generate-translate-zip'],
                                ['class' => 'btn btn-secondary']) ?>
                        </div>
                    </div>

                    <?php Pjax::begin(['id' => 'translate-table-pjax']); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'id',
                            'keyword',
                            'title_uz',
                            'title_oz',
                            'title_qr',
                            'title_ru',
                            //'created_at',
                            //'creator_id',
                            //'updated_at',
                            //'modifier_id',

                            [
                                'class' => '\backend\helper\ActionColumnCustom',
                                'template' => '{view} {update_translate} {delete}',
                            ],
                        ],
                        'summaryOptions' => ['class' => 'summary mb-2'],
                        'pager' => [
                            'class' => 'yii\bootstrap4\LinkPager',
                        ],
                        'tableOptions' => [
                            'id' => 'translate_index_table',
                            'class' => 'table table-striped table-bordered'
                        ]
                    ]); ?>
                    <?php Pjax::end();?>

                </div>
                <!--.card-body-->
            </div>
            <!--.card-->
        </div>
        <!--.col-md-12-->
    </div>
    <!--.row-->
</div>
