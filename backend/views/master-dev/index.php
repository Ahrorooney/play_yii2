<?php

use backend\helper\MasterDev;
use yii\widgets\ListView;

$this->title = 'Admin Senat';
$this->params['breadcrumbs'] = [['label' => $this->title]];

?>
<h4 class="ml-2">Database:</h4>
<div class="container-fluid">
    <!--    <div class="row">-->
    <!--        <div class="col-lg-6">-->
    <!--            --><?php //= \hail812\adminlte\widgets\Alert::widget([
    //                'type' => 'success',
    //                'body' => '<h3>Congratulations!</h3>',
    //            ]) ?>
    <!--            --><?php //= \hail812\adminlte\widgets\Callout::widget([
    //                'type' => 'danger',
    //                'head' => 'I am a danger callout!',
    //                'body' => 'There is a problem that we need to fix. A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.'
    //            ]) ?>
    <!--        </div>-->
    <!--    </div>-->

    <div class="row">
        <div class="col-md-4 col-sm-6 col-12">
            <h4><strong>Schemas:</strong></h4>
            <ol>
                <?php
                $schemas = MasterDev::getSchemasList();
                foreach ($schemas as $schema) {
                    echo "<li onClick='schemaClicked(\"".$schema['schema_name']."\")'>".$schema['schema_name']."</li>";
                } ?>
            </ol>
        </div>
        <div class="col-md-4 col-sm-6 col-12">
            <h4><strong>Info:</strong></h4>
            <div id="schema_info">
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-12">
            <h4><strong>Table Info:</strong></h4>
            <div id="schema_table_info">
            </div>
        </div>
    </div>
<!---->
<!--    <div class="row">-->
<!--        <div class="col-md-4 col-sm-6 col-12">-->
<!--            --><?php //= \hail812\adminlte\widgets\InfoBox::widget([
//                'text' => 'Get Cpu Vendor',
//                'number' => $system::getCpuVendor(),
//                'icon' => 'far fa-bookmark',
//                'progress' => [
//                    'width' => '70%',
//                    'description' => '70% Increase in 30 Days'
//                ]
//            ]) ?>
<!--        </div>-->
<!--        <div class="col-md-4 col-sm-6 col-12">-->
<!--            --><?php //$infoBox = \hail812\adminlte\widgets\InfoBox::begin([
//                'text' => 'Get Cpu Frequency',
//                'number' => $system::getCpuFreq(),
//                'theme' => 'success',
//                'icon' => 'far fa-thumbs-up',
//                'progress' => [
//                    'width' => '70%',
//                    'description' => '70% Increase in 30 Days'
//                ]
//            ]) ?>
<!--            --><?php //\hail812\adminlte\widgets\InfoBox::end() ?>
<!--        </div>-->
<!--        <div class="col-md-4 col-sm-6 col-12">-->
<!--            --><?php //= \hail812\adminlte\widgets\InfoBox::widget([
//                'text' => 'Get Cpu Vendor',
//                'number' => $system::getCpuArchitecture(),
//                'icon' => 'far fa-bookmark',
//            ]) ?>
<!--        </div>-->
<!--        <div class="col-md-4 col-sm-6 col-12">-->
<!--            --><?php //= \hail812\adminlte\widgets\InfoBox::widget([
//                'text' => 'Get Load',
//                'number' => $system::getLoad(),
//                'icon' => 'far fa-bookmark',
//            ]) ?>
<!--        </div>-->
<!--        <div class="col-md-4 col-sm-6 col-12">-->
<!--            --><?php //= \hail812\adminlte\widgets\InfoBox::widget([
//                'text' => 'Get Up Time',
//                'number' => $system::getUpTime(),
//                'icon' => 'far fa-bookmark',
//            ]) ?>
<!--        </div>-->
        <!--        <div class="col-md-4 col-sm-6 col-12">-->
        <!--            --><?php //= \hail812\adminlte\widgets\InfoBox::widget([
        //                'text' => 'Events',
        //                'number' => '41,410',
        //                'theme' => 'gradient-warning',
        //                'icon' => 'far fa-calendar-alt',
        //                'progress' => [
        //                    'width' => '70%',
        //                    'description' => '70% Increase in 30 Days'
        //                ],
        //                'loadingStyle' => true
        //            ]) ?>
        <!--        </div>-->
    </div>

</div>