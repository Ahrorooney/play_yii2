<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-md-12">
                            <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
                        </div>
                    </div>


                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            [
                                'attribute' => 'id',
                                'contentOptions' => ['style' => 'width:100px']
                            ],
                            [
                                'attribute' => 'pin',
                                'contentOptions' => ['style' => 'width:200px']
                            ],
                            'fullName',
                            [
                                'label' => 'Login',
                                'attribute' => 'username',
                            ],
                            [
                                'label' => 'Department',
                                'attribute' => 'departmentsAsBadges',
                                'format' => 'html'
                            ],
                            [
                                'label' => 'Roles',
                                'attribute' => 'rolesAsBadges',
                                'format' => 'html',
                            ],
                            //'image_path',
                            //'created_at',
                            //'creator_id',
                            //'updated_at',
                            //'modifier_id',

                            [
                                'class' => \backend\helper\ActionColumnCustom::class,
                                'template' => '{view}{view-full}{update}{delete}'
                            ],
                        ],
                        'summaryOptions' => ['class' => 'summary mb-2'],
                        'pager' => [
                            'class' => 'yii\bootstrap4\LinkPager',
                        ]
                    ]); ?>


                </div>
                <!--.card-body-->
            </div>
            <!--.card-->
        </div>
        <!--.col-md-12-->
    </div>
    <!--.row-->
</div>
