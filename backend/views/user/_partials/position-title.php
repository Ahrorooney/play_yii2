<?php

use backend\models\PositionTitleSearch;
use common\models\users\PositionTitle;
use common\models\users\User;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\grid\GridView;

/* @var PositionTitleSearch $dataProvider */
/* @var User $model */
?>

    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_position_title">Create</button>
    <hr>
<?php
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'id',
            'enableSorting' => false
        ],
        'user.fullName',
        [
            'attribute' => 'title_oz',
            'enableSorting' => false
        ],
        [
            'attribute' => 'title_uz',
            'enableSorting' => false
        ],
        [
            'attribute' => 'title_qr',
            'enableSorting' => false
        ],
        [
            'attribute' => 'title_ru',
            'enableSorting' => false
        ],
        //'created_at',
        //'creator_id',
        //'updated_at',
        //'modifier_id',

        [
            'class' => '\backend\helper\ActionColumnCustom',
            'template' => '{deletePositionTitle}',
        ],
    ],
    'summaryOptions' => ['class' => 'summary mb-2'],
    'pager' => [
        'class' => 'yii\bootstrap4\LinkPager',
    ]
]);
Modal::begin([
    'title' =>'<h2>Create User Position Title</h2>',
    'id' => 'modal_position_title',
    'size' => 'modal-lg',
]);

?>
<div class="position-link-form">

    <?php $form = ActiveForm::begin([
        'action' => '/position-title/create',
        'method' => 'POST'
    ]); ?>
    <?php
    $model_position_title = new PositionTitle();
    echo $form->field($model_position_title, 'user_id')->hiddenInput(['value' => $model->id])->label(false);
    ?>
    <h6><strong>User: </strong> <?php echo $model->fullName; ?></h6>

    <?= $form->field($model_position_title, 'title_oz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model_position_title, 'title_uz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model_position_title, 'title_qr')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model_position_title, 'title_ru')->textInput(['maxlength' => true]) ?>

    <div class="form-group" style="text-align: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
Modal::end();
?>

