<?php

use backend\models\UserPositionLinkSearch;
use common\models\users\User;
use common\models\users\UserPositionLink;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\grid\GridView;

/* @var UserPositionLinkSearch $dataProvider */
/* @var \common\models\users\User $model */

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserPositionLinkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_position">Create</button>
<hr>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'id',
            'enableSorting' => false
        ],
        'user.fullName',
        [
            'label' => 'Position',
            'value' => function ($model) {
                return @$model->position->title_oz;
            }
        ],
//        'created_at',
//        'creator_id',
        //'updated_at',
        //'modifier_id',

        [
            'class' => '\backend\helper\ActionColumnCustom',
            'template' => '{deleteUserPositionLink}',
        ],
    ],
    'summaryOptions' => ['class' => 'summary mb-2'],
    'pager' => [
        'class' => 'yii\bootstrap4\LinkPager',
    ]
]);

Modal::begin([
    'title' =>'<h2>Create User Position</h2>',
    'id' => 'modal_position',
    'size' => 'modal-lg',
]);

?>
<div class="user-role-link-form">

    <?php $form = ActiveForm::begin([
        'action' => '/user-position-link/create',
        'method' => 'POST'
    ]); ?>
    <?php
    $model_user_position_link = new UserPositionLink();
    echo $form->field($model_user_position_link, 'user_id')->hiddenInput(['value' => $model->id])->label(false);
    ?>
    <h6><strong>User: </strong> <?php echo $model->fullName; ?></h6>

    <?= $form->field($model_user_position_link, 'position_id')->widget(Select2::class, [
        'data' => $model_user_position_link->getPositionSelect(),
        'options' => [
            'placeholder' => 'Выберите '.$model_user_position_link->getAttributeLabel('position_id')
        ],
        'pluginOptions' => ['allowClear' => true],
    ]); ?>

    <div class="form-group" style="text-align: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
Modal::end();
?>

