<?php

use backend\models\UserRoleLinkSearch;
use common\models\users\User;
use common\models\users\UserRoleLink;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\grid\GridView;

/* @var UserRoleLinkSearch $dataProvider */
/* @var \common\models\users\User $model */
?>

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_role">Create</button>
<hr>
<?php
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'id',
            'enableSorting' => false
        ],
        'user.fullName',
        [
            'label' => 'Role',
            'value' => function ($model) {
                return @$model->role->title_oz;
            }
        ],
        [
            'label' => 'Department',
            'value' => function ($model) {
                return @$model->department->title_oz;
            }
        ],
//        'created_at',
        //'creator_id',
        //'updated_at',
        //'modifier_id',

        [
            'class' => '\backend\helper\ActionColumnCustom',
            'template' => '{deleteUserRoleLink}',
        ],
    ],
    'summaryOptions' => ['class' => 'summary mb-2'],
    'pager' => [
        'class' => 'yii\bootstrap4\LinkPager',
    ]
]);

Modal::begin([
    'title' =>'<h2>Create User Role</h2>',
    'id' => 'modal_role',
    'size' => 'modal-lg',
]);

?>
<div class="user-position-link-form">

    <?php $form = ActiveForm::begin([
        'action' => '/user-role-link/create',
        'method' => 'POST'
    ]); ?>
    <?php
        $model_user_link = new UserRoleLink();
        echo $form->field($model_user_link, 'user_id')->hiddenInput(['value' => $model->id])->label(false);
    ?>
        <h6><strong>User: </strong> <?php echo $model->fullName; ?></h6>

    <?= $form->field($model_user_link, 'role_id')->widget(Select2::class, [
        'data' => $model_user_link->getRolesSelect(),
        'options' => [
            'placeholder' => 'Выберите '.$model_user_link->getAttributeLabel('role_id')
        ],
        'pluginOptions' => ['allowClear' => true],
    ]); ?>

    <?= $form->field($model_user_link, 'department_id')->widget(Select2::class, [
        'data' => $model_user_link->getDepartmentsSelect(),
        'options' => [
            'placeholder' => 'Выберите '.$model_user_link->getAttributeLabel('department_id')
        ],
        'pluginOptions' => ['allowClear' => true],
    ]); ?>

    <div class="form-group" style="text-align: right;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
Modal::end();
