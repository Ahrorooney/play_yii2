<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \common\models\users\User */

?>

<div class="row">
    <div class="col-md-12">
        <p>
            <?= Html::a('Update', ['/user/update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['/user/delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'pin',
                'first_name',
                'surname',
                'middle_name',
                'image_path',
                'username',
                'created_at',
                'creator_id',
                'updated_at',
                'modifier_id',
            ],
        ]) ?>
    </div>
</div>