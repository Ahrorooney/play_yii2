<?php

/* @var $this yii\web\View */
/* @var $model \common\models\users\User */
/* @var $dataProviderURL \backend\models\UserRoleLinkSearch */
/* @var $dataProviderUPL \backend\models\UserPositionLinkSearch */
/* @var $dataProviderPT \backend\models\PositionTitleSearch */

 echo $this->render('_partials/user-info', [
                        'model' => $model
                    ]);
 ?><h3>User Role</h3><?php
 echo $this->render('_partials/user-role', [
     'model' => $model,
     'dataProvider' => $dataProviderURL
 ]);
?><h3>User Position</h3><?php
echo $this->render('_partials/user-position', [
    'model' => $model,
    'dataProvider' => $dataProviderUPL
]);
?><h3>User Position Title</h3><?php
echo $this->render('_partials/position-title', [
    'model' => $model,
    'dataProvider' => $dataProviderPT
]);