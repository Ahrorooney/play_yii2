<?php

use kartik\tabs\TabsX;

/* @var $this yii\web\View */
/* @var $model \common\models\users\User */

$this->title = $model->surname. ' '. $model->first_name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="container-fluid">

    <div class="card">
        <div class="card-body">
            <?php
            // Ajax Tabs Above
            echo TabsX::widget([
                'id' => 'user-tabs',
                'items'=>$items,
                'position'=>TabsX::POS_ABOVE,
                'encodeLabels'=>false
            ]);
            ?>
        </div>
        <!--.card-body-->
    </div>
    <!--.card-->
</div>