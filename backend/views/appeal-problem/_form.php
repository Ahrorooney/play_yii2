<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\helper\AppealProblem */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="appeal-problem-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title_oz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_uz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_qr')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sort')->textInput() ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>


    <?= $form->field($model, 'sphere_id')
        ->widget(Select2::class, [
            'data' => $model->getAppealSphereTitle(),
            'options' => [
                'placeholder' => 'Выберите '.$model->getAttributeLabel('sphere_id')
            ],
            'pluginOptions' => ['allowClear' => false],
        ]);
    ?>

<!--    --><?php //= $form->field($model, 'created_at')->textInput() ?>
<!---->
<!--    --><?php //= $form->field($model, 'creator_id')->textInput() ?>
<!---->
<!--    --><?php //= $form->field($model, 'updated_at')->textInput() ?>
<!---->
<!--    --><?php //= $form->field($model, 'modifier_id')->textInput() ?>
<!---->
<!--    --><?php //= $form->field($model, 'sphere_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
