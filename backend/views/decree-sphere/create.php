<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\helper\DecreeSphere */

$this->title = 'Create Decree Sphere';
$this->params['breadcrumbs'][] = ['label' => 'Decree Spheres', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <?=$this->render('_form', [
                        'model' => $model
                    ]) ?>
                </div>
            </div>
        </div>
        <!--.card-body-->
    </div>
    <!--.card-->
</div>