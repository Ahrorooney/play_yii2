<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DocumentTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Document Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-md-12">
                            <?= Html::a('Create Document Type', ['create'], ['class' => 'btn btn-success']) ?>
                        </div>
                    </div>


                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            [
                                'attribute' => 'id',
                                'contentOptions' => ['style' => 'width:100px']
                            ],
                            [
                                'label' => 'Parent id',
                                'value' => function ($model) {
                                    return @$model->parent->title_oz;
                                }
                            ],
                            [
                                'attribute' => 'title_oz',
                                'value' => function (\common\models\DocumentType $auth) {
                                    return Html::ul([
                                        'OZ - ' . $auth->title_oz,
                                        'UZ - ' . $auth->title_uz,
                                        'RU - ' . $auth->title_ru,
                                        'QR - ' . $auth->title_qr,
                                    ]);
                                },
                                'format' => 'raw',
                                'options' => [
                                    'class' => 'col-md-4'
                                ]
                            ],
                            //'created_at',
                            //'creator_id',
                            //'updated_at',
                            //'modifier_id',

                            ['class' => 'hail812\adminlte3\yii\grid\ActionColumn'],
                        ],
                        'summaryOptions' => ['class' => 'summary mb-2'],
                        'pager' => [
                            'class' => 'yii\bootstrap4\LinkPager',
                        ]
                    ]); ?>


                </div>
                <!--.card-body-->
            </div>
            <!--.card-->
        </div>
        <!--.col-md-12-->
    </div>
    <!--.row-->
</div>
