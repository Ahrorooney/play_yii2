<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\RegionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Regions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-md-12">
                            <?= Html::a('Create Regions', ['create'], ['class' => 'btn btn-success']) ?>
                        </div>
                    </div>


                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'id',
                            [
                                'label' => 'Parent',
                                'value' => function ($model) {
                                    return @$model->parent->title_uz;
                                }
                            ],
                            'title_oz',
                            'title_uz',
                            'title_qr',
                            //'title_ru',
                            //'sort',
                            //'level',
                            //'is_city:boolean',
                            //'soato',
                            //'creator_id',
                            //'created_at',
                            //'modifier_id',
                            //'updated_at',
                            //'origin_id',
                            //'code_lat',
                            //'code_cyr',
                            //'start_date',
                            //'finish_date',
                            //'order',
                            //'area_code',
                            //'country_id',
                            //'population',
                            //'map_id',

                            ['class' => 'hail812\adminlte3\yii\grid\ActionColumn'],
                        ],
                        'summaryOptions' => ['class' => 'summary mb-2'],
                        'pager' => [
                            'class' => 'yii\bootstrap4\LinkPager',
                        ]
                    ]); ?>


                </div>
                <!--.card-body-->
            </div>
            <!--.card-->
        </div>
        <!--.col-md-12-->
    </div>
    <!--.row-->
</div>
