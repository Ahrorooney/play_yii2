<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \common\models\users\Regions */

$this->title = $model->title_uz;
$this->params['breadcrumbs'][] = ['label' => 'Regions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <p>
                        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </p>
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            [
                                'label' => 'Parent',
                                'value' => function($model) {
                                    return @$model->parent->title_oz;
                                }
                            ],
                            'title_oz',
                            'title_uz',
                            'title_qr',
                            'title_ru',
                            'sort',
                            'level',
                            'is_city:boolean',
                            'soato',
                            [
                                'label' => 'Creator',
                                'value' => function($model) {
                                    return @$model->creator->username;
                                }
                            ],
                            'created_at',
                            [
                                'label' => 'Modifier',
                                'value' => function($model) {
                                    return @$model->modifier->username;
                                }
                            ],
                            'updated_at',
                            'origin_id',
                            'code_lat',
                            'code_cyr',
                            'start_date',
                            'finish_date',
                            'order',
                            'area_code',
                            'country_id',
                            'population',
                            'map_id',
                        ],
                    ]) ?>
                </div>
                <!--.col-md-12-->
            </div>
            <!--.row-->
        </div>
        <!--.card-body-->
    </div>
    <!--.card-->
</div>