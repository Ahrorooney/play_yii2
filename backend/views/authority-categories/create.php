<?php

/* @var $this yii\web\View */
/* @var $model \common\models\helper\AuthorityCategories */

$this->title = 'Create Authority Categories';
$this->params['breadcrumbs'][] = ['label' => 'Authority Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <?=$this->render('_form', [
                        'model' => $model
                    ]) ?>
                </div>
            </div>
        </div>
        <!--.card-body-->
    </div>
    <!--.card-->
</div>