<?php

use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\models\users\Authority */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="authority-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title_oz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_uz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_qr')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type_id')
        ->widget(Select2::class, [
            'data' => $model->getAuthorityTypesSelect(),
            'options' => [
                'placeholder' => 'Выберите '.$model->getAttributeLabel('type_id')
            ],
            'pluginOptions' => ['allowClear' => true],
        ]);
    ?>

    <?= $form->field($model, 'parent_id')
        ->widget(Select2::class, [
            'data' => $model->getAuthoritiesSelect(),
            'options' => [
                'placeholder' => 'Выберите '.$model->getAttributeLabel('parent_id')
            ],
            'pluginOptions' => ['allowClear' => true],
        ]);
    ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

<!--    --><?php //= $form->field($model, 'created_at')->textInput() ?>
<!---->
<!--    --><?php //= $form->field($model, 'creator_id')->textInput() ?>
<!---->
<!--    --><?php //= $form->field($model, 'updated_at')->textInput() ?>
<!---->
<!--    --><?php //= $form->field($model, 'modifier_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
