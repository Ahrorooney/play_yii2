<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \common\models\users\Authority */

$this->title = $model->title_oz;
$this->params['breadcrumbs'][] = ['label' => 'Authorities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <p>
                        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </p>
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'title_oz',
                            'title_uz',
                            'title_qr',
                            'title_ru',
                            [
                                'label' => 'Type id',
                                'value' => function ($model) {
                                    return @$model->authorityType->title_oz;
                                }
                            ],
                            [
                                'label' => 'Parent id',
                                'value' => function ($model) {
                                    return @$model->parent->title_oz;
                                }
                            ],
                            'code',
                            'created_at',
                            'creator_id',
                            'updated_at',
                            'modifier_id',
                            'origin_id',
                            [
                                'label' => 'Parent',
                                'attribute' => 'mainParent.title_uz',
                            ],
                            [
                                'label' => 'Territorial Type',
                                'attribute' => 'territorialityType.title_uz',
                            ],
                            [
                                'label' => 'Authority Category',
                                'attribute' => 'authorityCategories.title_uz',
                            ],
                            [
                                'label' => 'Authority Region',
                                'attribute' => 'regions.title_uz',
                            ],
                            [
                                'label' => 'Authority District',
                                'attribute' => 'district.title_uz',
                            ],
                            [
                                'label' => 'Belongs Region',
                                'attribute' => 'belongsRegion.title_uz',
                            ],
                            [
                                'label' => 'Belongs District',
                                'attribute' => 'belongsDistrict.title_uz',
                            ],
                            'code_name',
                            'inn',
                            [
                                'label' => 'Is Juridical',
                                'value' => function($model){
                                    if ($model->is_juridical){
                                        return 'ИСТИНА';
                                    } else {
                                        return 'ЛОЖЬ';
                                    }
                                }
                            ],
                            'level',
                        ],
                    ]) ?>
                </div>
                <!--.col-md-12-->
            </div>
            <!--.row-->
        </div>
        <!--.card-body-->
    </div>
    <!--.card-->
</div>