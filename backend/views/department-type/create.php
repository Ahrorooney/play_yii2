<?php

/* @var $this yii\web\View */
/* @var $model \common\models\users\DepartmentType */

$this->title = 'Create Department Type';
$this->params['breadcrumbs'][] = ['label' => 'Department Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <?=$this->render('_form', [
                        'model' => $model
                    ]) ?>
                </div>
            </div>
        </div>
        <!--.card-body-->
    </div>
    <!--.card-->
</div>