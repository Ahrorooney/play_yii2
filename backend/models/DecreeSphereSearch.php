<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\helper\DecreeSphere;

/**
 * DecreeSphereSearch represents the model behind the search form of `common\models\helper\DecreeSphere`.
 */
class DecreeSphereSearch extends DecreeSphere
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'sort', 'creator_id', 'modifier_id'], 'integer'],
            [['title_oz', 'title_uz', 'title_qr', 'title_ru', 'created_at', 'updated_at'], 'safe'],
            [['is_active'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DecreeSphere::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sort' => $this->sort,
            'is_active' => $this->is_active,
            'created_at' => $this->created_at,
            'creator_id' => $this->creator_id,
            'updated_at' => $this->updated_at,
            'modifier_id' => $this->modifier_id,
        ]);

        $query->andFilterWhere(['ilike', 'title_oz', $this->title_oz])
            ->andFilterWhere(['ilike', 'title_uz', $this->title_uz])
            ->andFilterWhere(['ilike', 'title_qr', $this->title_qr])
            ->andFilterWhere(['ilike', 'title_ru', $this->title_ru]);

        return $dataProvider;
    }
}
