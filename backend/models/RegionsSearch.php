<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * RegionsSearch represents the model behind the search form of `common\models\users\Regions`.
 */
class RegionsSearch extends \common\models\users\Regions
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'sort', 'level', 'soato', 'creator_id', 'created_at', 'modifier_id', 'updated_at', 'origin_id', 'order', 'area_code', 'country_id'], 'integer'],
            [['title_oz', 'title_uz', 'title_qr', 'title_ru', 'code_lat', 'code_cyr', 'start_date', 'finish_date', 'map_id'], 'safe'],
            [['is_city'], 'boolean'],
            [['population'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = \common\models\users\Regions::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'sort' => $this->sort,
            'level' => $this->level,
            'is_city' => $this->is_city,
            'soato' => $this->soato,
            'creator_id' => $this->creator_id,
            'created_at' => $this->created_at,
            'modifier_id' => $this->modifier_id,
            'updated_at' => $this->updated_at,
            'origin_id' => $this->origin_id,
            'order' => $this->order,
            'area_code' => $this->area_code,
            'country_id' => $this->country_id,
            'population' => $this->population,
        ]);

        $query->andFilterWhere(['ilike', 'title_oz', $this->title_oz])
            ->andFilterWhere(['ilike', 'title_uz', $this->title_uz])
            ->andFilterWhere(['ilike', 'title_qr', $this->title_qr])
            ->andFilterWhere(['ilike', 'title_ru', $this->title_ru])
            ->andFilterWhere(['ilike', 'code_lat', $this->code_lat])
            ->andFilterWhere(['ilike', 'code_cyr', $this->code_cyr])
            ->andFilterWhere(['ilike', 'start_date', $this->start_date])
            ->andFilterWhere(['ilike', 'finish_date', $this->finish_date])
            ->andFilterWhere(['ilike', 'map_id', $this->map_id]);

        return $dataProvider;
    }
}
