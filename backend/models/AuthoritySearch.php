<?php

namespace backend\models;

use common\models\users\Authority;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AuthoritySearch represents the model behind the search form of `common\models\users\Authority`.
 */
class AuthoritySearch extends Authority
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type_id', 'parent_id', 'creator_id', 'modifier_id'], 'integer'],
            [['title_oz', 'title_uz', 'title_qr', 'title_ru', 'code', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = \common\models\users\Authority::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type_id' => $this->type_id,
            'parent_id' => $this->parent_id,
            'created_at' => $this->created_at,
            'creator_id' => $this->creator_id,
            'updated_at' => $this->updated_at,
            'modifier_id' => $this->modifier_id,
        ]);

        $query->andFilterWhere([
            'OR',
            ['ilike', 'title_oz', $this->title_oz],
            ['ilike', 'title_uz', $this->title_oz],
            ['ilike', 'title_qr', $this->title_oz],
            ['ilike', 'title_ru', $this->title_oz]
        ]);

        return $dataProvider;
    }
}
