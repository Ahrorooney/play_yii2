<?php

namespace backend\models;

use common\models\helper\AuthorityCategories;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AuthorityCategoriesSearch represents the model behind the search form of `common\models\helper\AuthorityCategories`.
 */
class AuthorityCategoriesSearch extends AuthorityCategories
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'origin_id', 'code_name', 'sort', 'creator_id', 'modifier_id'], 'integer'],
            [['title_uz', 'title_oz', 'title_ru', 'title_qr', 'created_at', 'updated_at'], 'safe'],
            [['is_active'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AuthorityCategories::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'origin_id' => $this->origin_id,
            'code_name' => $this->code_name,
            'sort' => $this->sort,
            'is_active' => $this->is_active,
            'created_at' => $this->created_at,
            'creator_id' => $this->creator_id,
            'updated_at' => $this->updated_at,
            'modifier_id' => $this->modifier_id,
        ]);

        $query->andFilterWhere(['ilike', 'title_uz', $this->title_uz])
            ->andFilterWhere(['ilike', 'title_oz', $this->title_oz])
            ->andFilterWhere(['ilike', 'title_ru', $this->title_ru])
            ->andFilterWhere(['ilike', 'title_qr', $this->title_qr]);

        return $dataProvider;
    }
}
