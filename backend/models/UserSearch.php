<?php

namespace backend\models;

use common\models\users\User;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserSearch represents the model behind the search form of `common\models\users\User`.
 */
class UserSearch extends User
{
    public $fullName = null;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'creator_id', 'modifier_id'], 'integer'],
            [['pin', 'first_name', 'surname', 'middle_name', 'image_path', 'created_at', 'updated_at', 'fullName', 'username'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->setSort([
            'attributes' => [
                'id',
                'pin',
                'first_name',
                'surname',
                'middle_name',
                'image_path',
                'created_at',
                'creator_id',
                'updated_at',
                'modifier_id',
                'username',
                'fullName' => [
                    'asc' => ['first_name' => SORT_ASC, 'surname' => SORT_ASC, 'middle_name' => SORT_ASC],
                    'desc' => ['first_name' => SORT_DESC, 'surname' => SORT_DESC, 'middle_name' => SORT_DESC],
                    'label' => 'Full Name',
                    'default' => SORT_ASC
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'creator_id' => $this->creator_id,
            'updated_at' => $this->updated_at,
            'modifier_id' => $this->modifier_id,
        ]);

        $query->andFilterWhere(['ilike', 'pin', $this->pin])
            ->andFilterWhere(['ilike', 'image_path', $this->image_path])
            ->andFilterWhere(['ilike', 'username', $this->username]);

        $query->andFilterWhere([
            'OR',
            ['ilike', 'first_name', $this->fullName],
            ['ilike', 'surname', $this->fullName],
            ['ilike', 'middle_name', $this->fullName],
        ]);
        return $dataProvider;
    }
}
