<?php

namespace backend\helper;

use yii\db\Exception;

class MasterDev
{
    /**
     * @throws Exception
     */
    public static function getSchemaCount()
    {
        $res = \Yii::$app->db->createCommand('SELECT count(*) AS schema_count FROM information_schema.schemata;')
            ->queryAll();
        return $res[0]['schema_count'];
    }

    /**
     * @throws Exception
     */
    public static function getSchemasList()
    {
        $res = \Yii::$app->db->createCommand('SELECT * FROM information_schema.schemata;')
            ->queryAll();
        return $res;
    }

    public static function getSchemaTables($schema_name)
    {
        return \Yii::$app->db
            ->createCommand("SELECT * FROM information_schema.tables WHERE table_schema = '$schema_name'")
            ->queryAll();
    }

    public static function getTableColumns($schema_name, $table_name)
    {
//        $table_info = \Yii::$app->db
//            ->createCommand("SELECT pg_total_relation_size(relid) AS data_size FROM pg_catalog.pg_statio_user_tables
//                WHERE schemaname = '$schema_name' and relname = '$table_name'")
//            ->queryAll();
//        $table_rows = \Yii::$app->db
//            ->createCommand("SELECT count(*) AS exact_count FROM $schema_name.$table_name")
//            ->queryAll();
        $table_columns =  \Yii::$app->db
            ->createCommand("SELECT * FROM information_schema.columns WHERE table_schema = '$schema_name' and table_name = '$table_name'")
            ->queryAll();
        return ['table_columns' => $table_columns];
    }

}