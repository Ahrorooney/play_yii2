<?php

namespace backend\helper;

use hail812\adminlte3\yii\grid\ActionColumn;
use Yii;
use yii\bootstrap4\Button;
use yii\helpers\Html;

class ActionColumnCustom extends ActionColumn
{
    /**
     * Initializes the default button rendering callbacks.
     */
    protected function initDefaultButtons()
    {
        $this->initDefaultButton('view', 'eye');
        $this->initDefaultButton('update', 'pencil-alt');
        $this->initDefaultButton('update_translate', 'pencil-alt');
        $this->initDefaultButton('delete', 'trash', [
            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'data-method' => 'post',
        ]);
        $this->initDefaultButton('deleteUserRoleLink', 'trash', [
            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'data-method' => 'post',
        ]);
        $this->initDefaultButton('deleteUserPositionLink', 'trash', [
            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'data-method' => 'post',
        ]);
        $this->initDefaultButton('deletePositionTitle', 'trash', [
            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'data-method' => 'post',
        ]);
        $this->initDefaultButton('view-full', 'mountain');
    }

    /**
     * Initializes the default button rendering callback for single button.
     * @param string $name Button name as it's written in template
     * @param string $iconName The part of Bootstrap glyphicon class that makes it unique
     * @param array $additionalOptions Array of additional options
     * @since 2.0.11
     */
    protected function initDefaultButton($name, $iconName, $additionalOptions = [])
    {
        if (!isset($this->buttons[$name]) && strpos($this->template, '{' . $name . '}') !== false) {
            $this->buttons[$name] = function ($url, $model, $key) use ($name, $iconName, $additionalOptions) {

                switch ($name) {
                    case 'view':
                        $title = Yii::t('yii', 'View');
                        break;
                    case 'view-full':
                        $title = Yii::t('yii', 'Full View');
                        $iconName = 'mountain';
                        break;
                    case 'update':
                        $title = Yii::t('yii', 'Update');
                        break;
                    case 'update_translate':
                        $title = Yii::t('yii', 'Update');
                        $url = '#';
                        $additionalOptions['class'] = "update-btn";
                        break;
                    case 'delete':
                        $title = Yii::t('yii', 'Delete');
                        break;
                    case 'deleteUserRoleLink':
                        $url = str_replace("ajax", "user-role-link", $url);
                        $url = str_replace("deleteUserRoleLink", "delete", $url);
                        $title = Yii::t('yii', 'Delete');
                        break;
                    case 'deleteUserPositionLink':
                        $url = str_replace("ajax", "user-position-link", $url);
                        $url = str_replace("deleteUserPositionLink", "delete", $url);
                        $title = Yii::t('yii', 'Delete');
                        break;
                    case 'deletePositionTitle':
                        $url = str_replace("ajax", "position-title", $url);
                        $url = str_replace("deletePositionTitle", "delete", $url);
                        $title = Yii::t('yii', 'Delete');
                        break;
                    default:
                        $title = ucfirst($name);
                }
                $options = array_merge([
                    'title' => $title,
                    'aria-label' => $title,
                    'data-pjax' => '0',
                ], $additionalOptions, $this->buttonOptions);
                $icon = Html::tag('span', '', ['class' => "fas fa-$iconName"]);
                return Html::a($icon, $url, $options);
            };
        }
    }
}