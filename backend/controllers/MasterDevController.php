<?php

namespace backend\controllers;

use backend\helper\MasterDev;
use common\models\users\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

class MasterDevController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'ips' => ['::1'], // replace with your IP address
                    ],
                    [
                        'allow' => true,
                        'actions' => ['set-user-password'],
                        'roles' => ['?']

                    ],
                ],
                'denyCallback' => function () {
                    throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
                },
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGetSchemaTables()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax){
            return ['tables' => MasterDev::getSchemaTables(Yii::$app->request->post('schema_name'))];
        }
    }

    public function actionGetTableColumns()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax){
            return ['tables' => MasterDev::getTableColumns(Yii::$app->request->post('schema_name'),Yii::$app->request->post('table_name'))];
        }
    }

    public function actionGetTableSize()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax) {
            $schema_name = Yii::$app->request->post('schema_name');
            $table_name = Yii::$app->request->post('table_name');
            return \Yii::$app->db
                ->createCommand("SELECT pg_total_relation_size(relid) AS data_size FROM pg_catalog.pg_statio_user_tables 
                WHERE schemaname = '$schema_name' and relname = '$table_name'")
                ->queryAll();

        }
    }

    public function actionGetTableRowsCount()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax) {
            $schema_name = Yii::$app->request->post('schema_name');
            $table_name = Yii::$app->request->post('table_name');
            return \Yii::$app->db
            ->createCommand("SELECT count(*) AS exact_count FROM $schema_name.$table_name")
            ->queryAll();

        }
    }

    public function actionSetUserPassword()
    {
        $user = User::findOne(1);
        $user->setPassword('Qwerty123$');
        $user->save();
    }

}