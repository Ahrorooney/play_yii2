<?php

namespace backend\controllers;


use backend\models\PositionTitleSearch;
use backend\models\UserPositionLinkSearch;
use backend\models\UserRoleLinkSearch;
use common\models\helper\Translate;
use common\models\users\User;
use Yii;
use yii\filters\AccessControl;
use yii\rest\Controller;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class AjaxController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats'] = ['application/json' => Response::FORMAT_JSON];
        $behaviors[] = [
            'class' => 'yii\filters\AjaxFilter',
            'errorMessage' => 'Страница не найдена',
            'except' => ['document']
        ];

        $behaviors['access'] = [
            'class' => AccessControl::class,
            'rules' => [
                [
                    'allow' => false,
                    'roles' => ['?'], // Restrict access to non-authenticated users
                    'denyCallback' => function ($rule, $action) {
                        return $this->redirect(['site/login']); // Redirect to the login page
                    },
                ],
                [
                    'allow' => true,
                    'actions' => ['get-user-info', 'get-user-role', 'get-user-position', 'get-position-title',
                        'create-new-translate', 'get-translate-object', 'update-translate'],
                    'roles' => ['@'],
                ],
            ],
        ];
        return $behaviors;
    }

    public function actionGetUserInfo($id)
    {
        if (($model = User::findOne($id)) == null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $this->renderAjax('/user/_partials/user-info', [
            'model' => $model,
        ]);
    }

    public function actionGetUserRole($id)
    {
        if (($model = User::findOne($id)) == null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $searchModel = new UserRoleLinkSearch();
        $paramss = Yii::$app->request->queryParams;
        $paramss['UserRoleLinkSearch']['user_id'] = $id;
        $dataProvider = $searchModel->search($paramss);

        return $this->renderAjax('/user/_partials/user-role', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetUserPosition($id)
    {
        if (($model = User::findOne($id)) == null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $searchModel = new UserPositionLinkSearch();
        $paramss = Yii::$app->request->queryParams;
        $paramss['UserPositionLinkSearch']['user_id'] = $id;
        $dataProvider = $searchModel->search($paramss);

        return $this->renderAjax('/user/_partials/user-position', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetPositionTitle($id)
    {
        if (($model = User::findOne($id)) == null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $searchModel = new PositionTitleSearch();
        $paramss = Yii::$app->request->queryParams;
        $paramss['PositionTitleSearch']['user_id'] = $id;
        $dataProvider = $searchModel->search($paramss);

        return $this->renderAjax('/user/_partials/position-title', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreateNewTranslate()
    {
        $translate_model = new Translate();
        $translate_model->keyword = Yii::$app->request->post('keyword');
        $translate_model->title_uz = Yii::$app->request->post('translate_title_uz');
        $translate_model->title_oz = Yii::$app->request->post('translate_title_oz');
        $translate_model->title_qr = Yii::$app->request->post('translate_title_qr');
        $translate_model->title_ru = Yii::$app->request->post('translate_title_ru');
        if (!$translate_model->save()) {
            throw new NotAcceptableHttpException("Error on saving translate");
        }
        return True;
    }

    public function actionGetTranslateObject()
    {
        $translation_id = Yii::$app->request->post('id');
        return Translate::findOne($translation_id);
    }

    public function actionUpdateTranslate()
    {
        $translation_id = Yii::$app->request->post('id');
        $object = Translate::findOne($translation_id);
        $object->keyword = Yii::$app->request->post('translate_keyword');
        $object->title_uz = Yii::$app->request->post('translate_title_uz');
        $object->title_oz = Yii::$app->request->post('translate_title_oz');
        $object->title_qr = Yii::$app->request->post('translate_title_qr');
        $object->title_ru = Yii::$app->request->post('translate_title_ru');
        if (!$object->save()) {
            return [
                'success' => False,
                'message' => $object->errors
            ];
        }
        return [
            'success' => True,
            'message' => ''
        ];
    }

}