<?php

namespace backend\controllers;

use common\helpers\BaseController;
use common\models\users\UserPositionLink;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * UserPositionLinkController implements the CRUD actions for UserPositionLink model.
 */
class UserPositionLinkController extends BaseController
{

    /**
     * Creates a new UserPositionLink model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserPositionLink();
        $referrer_address = Yii::$app->request->referrer;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (str_contains($referrer_address, 'view-full')){
                return $this->redirect($referrer_address);
            }
            return $this->redirect(['/user/view', 'id' => $model->user_id, 'tab' => 4]);
        }

        return $this->redirect(['/user/view', 'id' => $model->user_id, 'tab' => 4]);
    }


    /**
     * Deletes an existing UserPositionLink model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        $user_id= $model->user_id;
        $model->delete();

        return $this->redirect(['/user/view', 'id' => $user_id, 'tab' => 4]);
    }

    /**
     * Finds the UserPositionLink model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return UserPositionLink the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserPositionLink::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
