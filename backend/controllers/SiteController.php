<?php

namespace backend\controllers;

use abhimanyu\systemInfo\SystemInfo;
use common\models\LoginForm;
use moonland\phpexcel\Excel;
use OpenSpout\Common\Exception\IOException;
use OpenSpout\Common\Exception\UnsupportedTypeException;
use OpenSpout\Reader\Exception\ReaderNotOpenedException;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

//use common\models\users\User;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => \yii\web\ErrorAction::class,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
//        $users_count = User::find()->count();
        return $this->render('index', [
            'users_count' => 0,
        ]);
    }

    /**
     * Login action.
     *
     * @return string|Response
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * @throws IOException
     * @throws Exception
     * @throws ReaderNotOpenedException
     * @throws UnsupportedTypeException
     */
//    public function actionReadExcelSheet1()
//    {
//        $transaction = Yii::$app->db->beginTransaction();
//        $filename = '1.xlsx';
//        $collection = (new FastExcel)->sheet(1)->import($filename)->all();
//        foreach ($collection as $col) {
//            $model = new AuthorityType();
//            $model->origin_id = $col['ID'];
//            $model->title_uz = $col['title_uz'];
//            $model->title_ru = $col['title_ru'];
//            $model->title_oz = $col['title_uz'];
//            if (!$model->save())
//            {
//                $transaction->rollBack();
//                var_dump($model->errors); die;
//            }
//        }
//        $model = new AuthorityType();
//        $model->origin_id = 4;
//        $model->title_oz = 'Boshqa';
//        $model->title_uz = 'Boshqa';
//        $model->title_ru = 'Boshqa';
//        if (!$model->save())
//        {
//            $transaction->rollBack();
//            var_dump($model->errors); die;
//        }
//        $transaction->commit();
//        return "<h1 style='color:darkgreen'>success!!!</h1>";
//
//    }
//
//    public function actionReadExcelSheet2()
//    {
//        $transaction = Yii::$app->db->beginTransaction();
//        $filename = '1.xlsx';
//        $collection = (new FastExcel)->sheet(2)->import($filename)->all();
//        foreach ($collection as $col) {
//            $model = new AuthorityCategories();
//            $model->origin_id = $col['ID'];
//            $model->title_uz = $col['title_uz'];
//            $model->title_ru = $col['title_ru'];
//            $model->title_oz = $col['title_uz'];
//            $model->order = $col['order'];
//            $model->code_name = $col['code_name'];
//            if (!$model->save())
//            {
//                $transaction->rollBack();
//                var_dump($model->errors); die;
//            }
//        }
//        $transaction->commit();
//        return "<h1 style='color:darkgreen'>success!!!</h1>";
//
//    }
//
//    public function actionReadExcelSheet3()
//    {
//        $transaction = Yii::$app->db->beginTransaction();
//        $filename = '1.xlsx';
//        $collection = (new FastExcel)->sheet(3)->import($filename)->all();
//        foreach ($collection as $col) {
//            $model = new TerritorialityType();
//            $model->origin_id = $col['ID'];
//            $model->title_uz = $col['title_uz'];
//            $model->title_ru = $col['title_ru'];
//            $model->title_oz = $col['title_uz'];
//            if (!$model->save())
//            {
//                $transaction->rollBack();
//                var_dump($model->errors); die;
//            }
//        }
//
//        $transaction->commit();
//        return "<h1 style='color:darkgreen'>success!!!</h1>";
//
//    }
//    public function actionReadExcelSheet4()
//    {
//        $transaction = Yii::$app->db->beginTransaction();
//        $filename = '1.xlsx';
//        $collection = (new FastExcel)->sheet(5)->import($filename)->all();
//        foreach ($collection as $col) {
//            $model = new Regions();
//            $model->origin_id = $col['id'];
//            $model->title_uz = $col['title_uz'];
//            $model->title_ru = $col['title_ru'];
//            $parent_object = Regions::findOne(['origin_id' => intval($col['parent_id'])]);
//            $model->parent_id = @$parent_object->id;
//            $model->code_lat = $col['code_lat'];
//            $model->code_cyr = $col['code_cyr'];
//            $model->start_date = $col['start_date'];
//            $model->finish_date = $col['finish_date'];
//            $model->order = $col['order'];
//            $model->area_code = $col['area_code'];
//            $model->country_id = $col['country_id'];
//            $model->population = $col['population'];
//            $model->map_id = $col['map_id'];
//            if (!$model->save())
//            {
//                $transaction->rollBack();
//                var_dump($model->errors); die;
//            }
//        }
//        $transaction->commit();
//        return "<h1 style='color:darkgreen'>success!!!</h1>";
//
//    }
//
//    public function actionReadExcelSheet5()
//    {
//        ini_set('memory_limit', '4096M');
//        ini_set('max_execution_time', 12000);
//        $transaction = Yii::$app->db->beginTransaction();
//        $filename = '1.xlsx';
//        $collection = (new FastExcel)->sheet(4)->import($filename)->all();
//        foreach ($collection as $col) {
//            $model = new Authority();
//            $model->origin_id = $col['id'];
//            $parent_object = Authority::findOne(['origin_id' => intval($col['parent_id'])]);
//            $model->parent_id = @$parent_object->id;
//            $main_parent_object = Authority::findOne(['origin_id' => intval($col['main_parent_id'])]);
//            $model->main_parent_id = @$main_parent_object->id;
//            if ($col['type_id'] == null){
//                $type_object = AuthorityType::findOne(['origin_id' => 4]);
//            }else{
//                $type_object = AuthorityType::findOne(['origin_id' => intval($col['type_id'])]);
//            }
//            $model->type_id = $type_object->id;
//            $territoriality_type_object = TerritorialityType::findOne(['origin_id' => intval($col['territoriality_type_id'])]);
//            $model->territoriality_type_id = @$territoriality_type_object->id;
//            $category_object = AuthorityCategories::findOne(['origin_id' => intval($col['category_id'])]);
//            $model->category_id = @$category_object->id;
//            $region_object = Regions::findOne(['origin_id' => intval($col['region_id'])]);
//            $model->region_id = @$region_object->id;
//            $district_object = Regions::findOne(['origin_id' => intval($col['district_id'])]);
//            $model->district_id = @$district_object->id;
//            $b_region_object = Regions::findOne(['origin_id' => intval($col['belongs_region_id'])]);
//            $model->belongs_region_id = @$b_region_object->id;
//            $b_district_object = Regions::findOne(['origin_id' => intval($col['belongs_district_id'])]);
//            $model->belongs_district_id = @$b_district_object->id;
//            $model->code_name = $col['code_name'];
//            $model->code = $col['code_name'];
//            $model->title_uz = $col['title_uz'];
//            $model->title_oz = $col['title_oz'];
//            $model->title_ru = $col['title_ru'];
//            $model->inn = $col['inn'];
//            $model->is_juridical = $col['is_juridical'];
//            $model->level = $col['LEVEL'];
//            if (!$model->save())
//            {
//                $transaction->rollBack();
//                var_dump($model->errors); die;
//            }
//        }
//        $transaction->commit();
//        return "<h1 style='color:darkgreen'>success!!!</h1>";
//
//    }
}
