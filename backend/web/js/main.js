$(document).ready(function() {
    // Find all nav-link elements inside the #user-tabs element
    let $userTabs = $('#user-tabs');
    let $navLinks = $userTabs.find('.nav-link');

    // Find the currently active nav-link element and trigger its click event
    let $activeNavLink = $navLinks.filter('.active');
    $activeNavLink.click();

    updateRowFunction();
    $(document).on('ready pjax:end', function (event) {
        updateRowFunction();
    });

});
var row = '';
const updateRowFunction = () => {

    $('.update-btn').click(function(e) {
        if ($('#translate_index_table').hasClass('updating_row')) {
            next_row = row.next()
            next_row.addClass('red-border');

            setTimeout(function() {
                next_row.removeClass('red-border')
            }, 1000);
        } else {
            $('#translate_index_table').addClass('updating_row')

            e.preventDefault();
            row = $(this).closest('tr');

            let count_i = 0
            let new_row_values = []
            // Iterate over each cell in the row
            row.find('td').each(function () {
                if (count_i > 0 && count_i < 7) {
                    new_row_values.push($(this).text());
                }
                count_i += 1
            });

            let newRow = '<tr data-key="2">' +
                '<td></td>' +
                '<td><h5><span class="badge badge-pill badge-primary">Updating ' + new_row_values[0] + '</span></h5></td>' +
                '<td><input type="text" class="form-control" name="translate_keyword" value="' + new_row_values[1] + '"></td>' +
                '<td><input type="text" class="form-control" name="translate_title_uz" value="' + new_row_values[2] + '"></td>' +
                '<td><input type="text" class="form-control" name="translate_title_oz" value="' + new_row_values[3] + '"></td>' +
                '<td><input type="text" class="form-control" name="translate_title_qr" value="' + new_row_values[4] + '"></td>' +
                '<td><input type="text" class="form-control" name="translate_title_ru" value="' + new_row_values[5] + '"></td>' +
                '<td><a href="#" class="send_update_translate" onclick="send_update_translate(' + new_row_values[0] + ')">' +
                '<span class="fas fa-check-circle"></span></a></td></tr>';
            row.after(newRow).next().hide().fadeIn(500);
        }
    });
}

const send_update_translate = (id) => {
    let data = {
        id: id,
        translate_keyword: $('input[name="translate_keyword"]').val(),
        translate_title_uz: $('input[name="translate_title_uz"]').val(),
        translate_title_oz: $('input[name="translate_title_oz"]').val(),
        translate_title_qr: $('input[name="translate_title_qr"]').val(),
        translate_title_ru: $('input[name="translate_title_ru"]').val()
    };

    $.ajax({
        url: '/ajax/update-translate',
        type: 'POST',
        data: data,
        success: function(response) {
            // Handle the response from the server
            if (response['success']) {
                $.pjax.reload({container: '#translate-table-pjax'});
            } else {
                alert(response['message'])
            }
        },
        error: function(xhr, status, error) {
            // Handle errors
            console.log(error);
        }
    });

};

const schemaClicked = (schema_name) => {
        $.ajax({
            type: 'POST',
            url: '/master-dev/get-schema-tables',
            data: {
                'schema_name': schema_name
            },
            success: function (data) {
                $('#schema_info').html('');
                html_string = '<ol>';
                $.each(data.tables, function(key, value) {
                    html_string += '<li onClick="getTableColumnsClicked(\''+schema_name+'\', \''+value['table_name']+'\')">'+value['table_name']+"</li>";
                });
                html_string += '</ol>';
                $('#schema_info').html(html_string);
            },
        });
}

const getTableColumnsClicked = (schema_name, table_name) => {
    $.ajax({
        type: 'POST',
        url: '/master-dev/get-table-columns',
        data: {
            'schema_name': schema_name,
            'table_name': table_name
        },
        success: function (data) {
            $('#schema_table_info').html('');
            html_string = '<p><strong id="table_size_info" onClick="getTableSizeClicked(\''+schema_name+'\', \''+table_name+'\')">Size: </strong></p>';
            html_string += '<p><strong id="table_rows_count_info" onClick="getTableRowsCountClicked(\''+schema_name+'\', \''+table_name+'\')\">Rows count: </strong></p>';
            html_string += '<ol>';
            html_string += "<h6><strong>column_name</strong>-<span style='color:indianred'>is_nullable</span>-" +
                "<span style='color:#0b2e13'>data_type</span>-<span style='color:lightskyblue'>default_val</span></h6>"
            $.each(data.tables.table_columns, function(key, value) {
                html_string += "<li><strong>"+value['column_name']+":</strong>-"+
                    "<span style='color:indianred'>"+value['is_nullable']+"</span>"+"-"+
                    "<span style='color:#0b2e13'>" + value['data_type']+ "</span>" + "-"+
                    "<span style='color:lightskyblue'>"+value['column_default']+"</span></li>";
            });
            html_string += '</ol>';
            $('#schema_table_info').html(html_string);
        },
    });
}

const getTableSizeClicked = (schema_name, table_name) => {
    $.ajax({
        type: 'POST',
        url: '/master-dev/get-table-size',
        data: {
            'schema_name': schema_name,
            'table_name': table_name
        },
        success: function (data) {
            $('#table_size_info').html('');
            html_string = "<strong>Size: "+(data[0]['data_size'] / (1024 * 1024))+"MB</strong>";
            $('#table_size_info').html(html_string);
        },
    });
}

const getTableRowsCountClicked = (schema_name, table_name) => {
    $.ajax({
        type: 'POST',
        url: '/master-dev/get-table-rows-count',
        data: {
            'schema_name': schema_name,
            'table_name': table_name
        },
        success: function (data) {
            $('#table_rows_count_info').html('');
            html_string = "<p><strong>Rows count: "+data[0]['exact_count']+"</strong></p>";
            $('#table_rows_count_info').html(html_string);
        },
    });
}