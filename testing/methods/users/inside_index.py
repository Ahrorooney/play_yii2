import time

from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from colorama import Fore, Style
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

from methods.base.BaseMethods import BaseMethods


class InsideIndex(BaseMethods):

    def __init__(self) -> None:
        super().__init__(without_window=True)
        self.user_id = None
        self.user_first_name = 'Wayne'
        self.user_surname = 'Rooney'
        self.user_middle_name = 'Wazza'
        self.user_pin = 1010101010

    def test_go_to_user_index(self):

        """tests going to user index
        """

        self.driver.get(self.base_url + "user")
        self.driver.implicitly_wait(5)

        title = self.driver.title
        assert title == "Users"

        print(Fore.GREEN + "test_enter_user_index passed successfully!" + Style.RESET_ALL)

    def test_create_user(self):
        """
        tests going to user/create and create user
        *after finishing the scenario, added records will be deleted!*
        :return:
        """
        create_btn = self.driver.find_element(By.CSS_SELECTOR, 'a[href="/user/create"]')
        create_btn.click()
        title = self.driver.title
        assert title == "Create User"
        print(Fore.GREEN + "inside user create page!" + Style.RESET_ALL)

        input_pin = self.driver.find_element(By.ID, 'user-pin')
        input_pin.send_keys(self.user_pin)
        input_first_name = self.driver.find_element(By.ID, 'user-first_name')
        input_first_name.send_keys(self.user_first_name)
        input_surname = self.driver.find_element(By.ID, 'user-surname')
        input_surname.send_keys(self.user_surname)
        input_middle_name = self.driver.find_element(By.ID, 'user-middle_name')
        input_middle_name.send_keys(self.user_middle_name)
        input_image_path = self.driver.find_element(By.ID, 'user-image_path')
        input_image_path.send_keys('test_image_path')
        input_pin.send_keys(Keys.ENTER)
        wait = WebDriverWait(self.driver, 10)
        wait.until(expected_conditions.url_changes(self.driver.current_url))

        # this line should be added in every create operation. Because, later it is required to delete temporary data.
        self.add_db_change('create', 'users.users')

        title = self.driver.title
        assert title == self.user_surname+" "+self.user_first_name
        print(Fore.GREEN + "inside user create page!" + Style.RESET_ALL)

    def test_find_row_just_created_user(self):
        """
        going to user_view
        :return:
        """
        self.test_go_to_user_index()
        full_name = self.user_surname + ' ' + self.user_first_name + ' ' + self.user_middle_name
        row = self.driver.find_element(By.XPATH, "//tr[td[contains(text(), '"+full_name+"')]]")
        a_tag_view = row.find_element(By.XPATH, ".//td[last()]//a[.//span[contains(@class, 'fas fa-eye')]]")
        a_tag_view.click()

        self.driver.implicitly_wait(5)

        title = self.driver.title
        assert title == self.user_surname + " " + self.user_first_name

        print(Fore.GREEN + "test_find_row_just_created_user passed successfully!" + Style.RESET_ALL)

    def test_check_user_info(self):
        """
        after test_find_row_just_created_user, check user info
        :return:
        """

        title = self.driver.title
        assert title == self.user_surname + " " + self.user_first_name

        # now check user_info first name, pin and store user_id
        row = self.driver.find_element(By.XPATH, "//tr[th[contains(text(), 'First Name')]]")
        last_td_element = row.find_elements(By.TAG_NAME, 'td')[-1]
        assert last_td_element.text == self.user_first_name

        row = self.driver.find_element(By.XPATH, "//tr[th[contains(text(), 'Pin')]]")
        last_td_element = row.find_elements(By.TAG_NAME, 'td')[-1]
        assert int(last_td_element.text) == self.user_pin

        row = self.driver.find_element(By.XPATH, "//tr[th[contains(text(), 'ID')]]")
        last_td_element = row.find_elements(By.TAG_NAME, 'td')[-1]
        self.user_id = int(last_td_element.text)

        print(Fore.GREEN + "test_check_user_info passed successfully!" + Style.RESET_ALL)

    def test_update_user(self):
        """
        this test checks user update (pin => 11111111, last_name => Roooney)
        :return:
        """
        title = self.driver.title
        assert title == self.user_surname + " " + self.user_first_name

        url = "/user/update?id=" + str(self.user_id)

        update_btn = self.driver.find_element(By.CSS_SELECTOR, 'a[href="'+url+'"]')
        update_btn.click()
        title = self.driver.title
        assert title == "Update User: "+str(self.user_id)

        pin_field = self.driver.find_element(By.ID, 'user-pin')
        pin_field.clear()
        pin_field.send_keys(11111111)
        self.user_pin = 11111111

        surname_field = self.driver.find_element(By.ID, 'user-surname')
        surname_field.clear()
        surname_field.send_keys('Roooney')
        self.user_surname = 'Roooney'

        form = self.driver.find_element(By.ID, 'user_create_form')
        form.submit()

        wait = WebDriverWait(self.driver, 10)
        wait.until(expected_conditions.url_changes(self.driver.current_url))

        title = self.driver.title
        assert title == self.user_surname + " " + self.user_first_name

        # now check user_info first name, pin and store user_id
        row = self.driver.find_element(By.XPATH, "//tr[th[contains(text(), 'Surname')]]")
        last_td_element = row.find_elements(By.TAG_NAME, 'td')[-1]
        assert last_td_element.text == self.user_surname

        row = self.driver.find_element(By.XPATH, "//tr[th[contains(text(), 'Pin')]]")
        last_td_element = row.find_elements(By.TAG_NAME, 'td')[-1]
        assert int(last_td_element.text) == self.user_pin

        print(Fore.GREEN + "test_update_user passed successfully!" + Style.RESET_ALL)

    def test_delete_user(self):
        title = self.driver.title
        assert title == self.user_surname + " " + self.user_first_name

        url = "/user/delete?id=" + str(self.user_id)

        delete_button = self.driver.find_element(By.CSS_SELECTOR, 'a[href="' + url + '"]')
        delete_button.click()

        alert = self.driver.switch_to.alert
        alert.accept()

        self.driver.implicitly_wait(5)
        self.delete_from_json('users.users')

        self.test_go_to_user_index()
        full_name = self.user_surname + ' ' + self.user_first_name + ' ' + self.user_middle_name
        try:
            self.driver.find_element(By.XPATH, "//tr[td[contains(text(), '" + full_name + "')]]")
        except NoSuchElementException:
            print(Fore.GREEN + "test_delete_user passed successfully!" + Style.RESET_ALL)

    def test_go_to_user_role_in_user_tab(self):
        self.test_find_row_just_created_user()

        user_role_tab = self.driver.find_element(By.CSS_SELECTOR, 'a[href="#user-tabs-tab1"]')
        user_role_tab.click()

        self.driver.implicitly_wait(5)

        class_value = user_role_tab.get_attribute('class')
        assert 'active' in class_value
        print(Fore.GREEN + "test_go_to_user_role_in_user_tab passed successfully!" + Style.RESET_ALL)

