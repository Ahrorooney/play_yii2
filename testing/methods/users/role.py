import time

from selenium.webdriver.common.by import By
from colorama import Fore, Style
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from methods.base.BaseMethods import BaseMethods


class Role:

    def __init__(self, driver, base_url) -> None:
        self.driver = driver
        self.base_url = base_url
        self.role_id = 0

    def test_go_to_role(self):
        """
        tests going to 'role'
        :return:
        """
        self.driver.get(self.base_url + "role")
        self.driver.implicitly_wait(5)
        assert self.driver.title == "Roles"
        print(Fore.GREEN + "test_go_to_role passed successfully!" + Style.RESET_ALL)

    def test_create_role(self):
        """
        tests creates news role
        :return:
        """
        self.test_go_to_role()
        create_role = self.driver.find_element(By.CSS_SELECTOR, 'a[href="/role/create"]')
        create_role.click()
        self.driver.implicitly_wait(5)
        assert self.driver.title == 'Create Role'

        role_title_oz = self.driver.find_element(By.ID, 'role-title_oz')
        role_title_oz.send_keys('role_title_oz')
        role_title_uz = self.driver.find_element(By.ID, 'role-title_uz')
        role_title_uz.send_keys('role_title_uz')
        role_title_qr = self.driver.find_element(By.ID, 'role-title_qr')
        role_title_qr.send_keys('role_title_qr')
        role_title_ru = self.driver.find_element(By.ID, 'role-title_ru')
        role_title_ru.send_keys('role_title_ru')

        create_role_form = self.driver.find_element(By.CSS_SELECTOR, 'form[action="/role/create"]')
        create_role_form.submit()

        wait = WebDriverWait(self.driver, 10)
        wait.until(expected_conditions.url_changes(self.driver.current_url))

        assert self.driver.title == 'role_title_oz'
        row = self.driver.find_element(By.XPATH, "//tr[th[contains(text(), 'ID')]]")
        last_td_element = row.find_elements(By.TAG_NAME, 'td')[-1]
        self.role_id = int(last_td_element.text)
        BaseMethods.add_db_change('create', 'users.role')

        print(Fore.GREEN + "test_create_role passed successfully!" + Style.RESET_ALL)

    def test_delete_role(self):
        assert self.driver.title == 'role_title_oz'

        url = "/role/delete?id=" + str(self.role_id)

        delete_button = self.driver.find_element(By.CSS_SELECTOR, 'a[href="' + url + '"]')
        delete_button.click()

        alert = self.driver.switch_to.alert
        alert.accept()

        self.driver.implicitly_wait(5)

        BaseMethods.delete_from_json('users.role')

        check_url = self.base_url + "role/view?id=" + str(self.role_id)

        self.driver.get(check_url)
        self.driver.implicitly_wait(5)

        assert self.driver.title == 'Not Found (#404)'

        print(Fore.GREEN + "test_delete_role passed successfully!" + Style.RESET_ALL)

