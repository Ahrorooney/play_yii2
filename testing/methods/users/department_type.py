import time

from selenium.webdriver.common.by import By
from colorama import Fore, Style
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from methods.base.BaseMethods import BaseMethods


class DepartmentType:

    def __init__(self, driver, base_url) -> None:
        self.driver = driver
        self.base_url = base_url
        self.role_id = 0

    def test_go_to_department_type(self):
        """
        tests going to 'department_type'
        :return:
        """
        self.driver.get(self.base_url + "department-type")
        self.driver.implicitly_wait(5)
        assert self.driver.title == "Department Types"
        print(Fore.GREEN + "test_go_to_department_type passed successfully!" + Style.RESET_ALL)

    def test_create_department_type(self):
        """
        tests creates news department_type
        :return:
        """
        self.test_go_to_department_type()
        create_department_type = self.driver.find_element(By.CSS_SELECTOR, 'a[href="/department-type/create"]')
        create_department_type.click()
        self.driver.implicitly_wait(5)
        assert self.driver.title == 'Create Department Type'

        departmenttype_title_oz = self.driver.find_element(By.ID, 'departmenttype-title_oz')
        departmenttype_title_oz.send_keys('departmenttype_title_oz')
        departmenttype_title_uz = self.driver.find_element(By.ID, 'departmenttype-title_uz')
        departmenttype_title_uz.send_keys('departmenttype_title_uz')
        departmenttype_title_qr = self.driver.find_element(By.ID, 'departmenttype-title_qr')
        departmenttype_title_qr.send_keys('departmenttype_title_qr')
        departmenttype_title_ru = self.driver.find_element(By.ID, 'departmenttype-title_ru')
        departmenttype_title_ru.send_keys('departmenttype_title_ru')

        create_department_type_form = self.driver.find_element(By.CSS_SELECTOR, 'form[action="/department-type/create"]')
        create_department_type_form.submit()

        wait = WebDriverWait(self.driver, 10)
        wait.until(expected_conditions.url_changes(self.driver.current_url))

        assert self.driver.title == 'departmenttype_title_oz'
        row = self.driver.find_element(By.XPATH, "//tr[th[contains(text(), 'ID')]]")
        last_td_element = row.find_elements(By.TAG_NAME, 'td')[-1]
        self.department_type_id = int(last_td_element.text)
        BaseMethods.add_db_change('create', 'users.department_type')

        print(Fore.GREEN + "test_create_department_type passed successfully!" + Style.RESET_ALL)

    def test_delete_department_type(self):
        assert self.driver.title == 'departmenttype_title_oz'

        url = "/department-type/delete?id=" + str(self.department_type_id)

        delete_button = self.driver.find_element(By.CSS_SELECTOR, 'a[href="' + url + '"]')
        delete_button.click()

        alert = self.driver.switch_to.alert
        alert.accept()

        self.driver.implicitly_wait(5)

        BaseMethods.delete_from_json('users.department_type')

        check_url = self.base_url + "department-type/view?id=" + str(self.department_type_id)

        self.driver.get(check_url)
        self.driver.implicitly_wait(5)

        assert self.driver.title == 'Not Found (#404)'

        print(Fore.GREEN + "test_delete_department_type passed successfully!" + Style.RESET_ALL)

