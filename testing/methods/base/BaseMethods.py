import json
import os
import time
from pathlib import Path

import psycopg2 as psycopg2
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from colorama import Fore, Style
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.chrome.options import Options


class BaseMethods:

    def __init__(self, without_window=False) -> None:
        self.driver = None
        # Site's base url, you can change this line when change testing environment
        self.base_url = 'http://backend-senat.lo/'
        self.without_window = without_window
        self.setup_method()

    def setup_method(self):
        # This option is for toggle visibility of the browser during test
        self.chrome_options = Options()
        if self.without_window:
            self.chrome_options.add_argument("--headless")

        # Opening browser for testing purpose
        self.driver = webdriver.Chrome('C:\chromedriver\chromedriver.exe', options=self.chrome_options)

        # Maximize browser's view during test
        self.driver.maximize_window()
        # Kind of timeout that waits until element is loaded fully before starting executing next lines
        self.driver.implicitly_wait(0.5)

    def teardown_method(self):
        """Closes browser and exits driver"""
        self.driver.quit()

    def test_go_to_site_index(self):
        """Tests entering to the first page of the Admin Page
        """

        self.driver.get(self.base_url+"site/login")

        self.driver.implicitly_wait(0.5)
        # check if title is same as the first page of the site
        title = self.driver.title
        assert title == "Login"
        print(Fore.GREEN + "test_site_index passed successfully!" + Style.RESET_ALL)

    def test_login(self, username, password):
        """tests signing in to Admin Page using username and password
        Args:
            username (string)
            password (string)
        """

        self.driver.get(self.base_url+"site/login")

        username_input = self.driver.find_element(By.ID, "loginform-username")
        username_input.send_keys(username)

        password_input = self.driver.find_element(By.ID, 'loginform-password')
        password_input.send_keys(password)

        form_login = self.driver.find_element(By.ID, 'login-form')
        form_login.submit()

        wait = WebDriverWait(self.driver, 5)
        wait.until(expected_conditions.url_changes(self.driver.current_url))
        assert self.driver.title == "Admin Senat"
        print(Fore.GREEN + "test_login passed successfully!" + Style.RESET_ALL)

    def test_logout(self):
        """
            tests logout
        """

        sign_out_i = self.driver.find_element(By.CLASS_NAME, 'fa-sign-out-alt')
        a_tag = sign_out_i.find_element(By.XPATH, "..")
        a_tag.click()
        self.driver.implicitly_wait(5)
        title = self.driver.title
        assert title == "Login"
        print(Fore.GREEN + "test_logout passed successfully!" + Style.RESET_ALL)

    @staticmethod
    def add_db_change(modification_type, schema_table):
        # Get the user's home directory
        home_dir = str(Path.home())

        # Create the file path
        file_path = os.path.join(home_dir, 'senat_test_changes_db.json')

        # Check if the file exists
        if not os.path.exists(file_path):
            # File does not exist, create it
            data = {'test': 'test'}
            with open(file_path, 'w') as file:
                json.dump(data, file)

        # File exists, open it
        with open(file_path, 'r') as file:
            data = json.load(file)

        if modification_type not in data:
            data[modification_type] = [schema_table]
        else:
            data[modification_type].append(schema_table)
        with open(file_path, 'w') as file:
            json.dump(data, file)

    def delete_test_changes_fromo_db(self):
        # Get the user's home directory
        home_dir = str(Path.home())

        # Create the file path
        file_path = os.path.join(home_dir, 'senat_test_changes_db.json')

        # File exists, open it
        with open(file_path, 'r') as file:
            data = json.load(file)

        for schema_table in data['create']:
            self.delete_the_last_row_from_table(schema_table)

    def delete_the_last_row_from_table(self, schema_table, primary_key='id'):
        # Establish a connection to the PostgreSQL database
        conn = psycopg2.connect(
            host="localhost",
            database="senat_admin",
            user="postgres",
            password="postgres"
        )

        # Create a cursor object to execute SQL queries
        cursor = conn.cursor()

        # Define the table name and the primary key column name
        table_name = schema_table
        primary_key_column = primary_key

        # Retrieve the last row's primary key value
        cursor.execute(f"SELECT {primary_key_column} FROM {table_name} ORDER BY {primary_key_column} DESC LIMIT 1")
        result = cursor.fetchone()
        last_row_id = result[0] if result else None

        # Delete the last row if it exists
        if last_row_id:
            delete_query = f"DELETE FROM {table_name} WHERE {primary_key_column} = %s"
            cursor.execute(delete_query, (last_row_id,))
            conn.commit()
            print(f"Last row (ID: {last_row_id}) deleted successfully.")
            self.delete_from_json(schema_table)
        else:
            print("No rows found in the table.")

        # Close the cursor and the database connection
        cursor.close()
        conn.close()

    @staticmethod
    def delete_from_json(schema_table):
        # Get the user's home directory
        home_dir = str(Path.home())

        # Create the file path
        file_path = os.path.join(home_dir, 'senat_test_changes_db.json')

        # File exists, open it
        with open(file_path, 'r') as file:
            data = json.load(file)
        if schema_table in data['create']:
            index = data['create'].index(schema_table)
            data['create'].pop(index)

        with open(file_path, 'w') as file:
            json.dump(data, file)
            print("Json modified")
