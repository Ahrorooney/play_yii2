from colorama import Fore, Style

from methods.base.BaseMethods import BaseMethods


class AuthenticationErrors(BaseMethods):
    def __init__(self, driver):
        super().__init__()
        self.driver = driver

    def test_controller_without_login(self, controller_key):
        """tests entering to the given controller without login
        Args:
            controller_key (string)
                """
        self.driver.get(self.base_url + controller_key)

        self.driver.implicitly_wait(0.5)
        # check if title is same as the first page of the site
        title = self.driver.title
        assert title == "Login"
        print(Fore.GREEN + "Entering without login to "+controller_key+" passed successfully!" + Style.RESET_ALL)
