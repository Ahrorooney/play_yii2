from methods.users.department_type import DepartmentType
from methods.users.inside_index import InsideIndex
from methods.users.role import Role

user_inside_index = InsideIndex()
user_inside_index.test_login('username1', 'Qwerty123$')
user_inside_index.test_go_to_user_index()
user_inside_index.test_create_user()
user_inside_index.test_find_row_just_created_user()
user_inside_index.test_check_user_info()
user_inside_index.test_update_user()
user_inside_index.test_delete_user()
user_inside_index.test_create_user()

# test Role create, delete operations, and lastly create a new role
role = Role(user_inside_index.driver, user_inside_index.base_url)
role.test_go_to_role()
role.test_create_role()
role.test_delete_role()
role.test_create_role()

# test Department Type create, delete operations, and lastly create a new department type
department_type = DepartmentType(user_inside_index.driver, user_inside_index.base_url)
department_type.test_go_to_department_type()
department_type.test_create_department_type()
department_type.test_delete_department_type()
department_type.test_create_department_type()

# test Department create, delete operations, and lastly create a new department type

user_inside_index.test_go_to_user_role_in_user_tab()
