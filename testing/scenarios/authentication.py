from methods.base.BaseMethods import BaseMethods
from methods.errors_check.AuthenticationErrors import AuthenticationErrors

base = BaseMethods()

base.test_login('username1', 'Qwerty123$')
base.driver.implicitly_wait(0.5)

base.test_logout()
base.driver.implicitly_wait(0.5)

authentication_errors = AuthenticationErrors(base.driver)
authentication_errors.test_controller_without_login('user')
authentication_errors.test_controller_without_login('authority')
authentication_errors.test_controller_without_login('translate')
authentication_errors.driver.implicitly_wait(0.5)
base.teardown_method()
