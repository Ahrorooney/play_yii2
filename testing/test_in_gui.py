import os
import subprocess
import sys
import tkinter as tk
from tkinter import ttk


# Dictionary containing scenarios
scenario = {
    "Go to site": "C:\\Users\\a_gaibnazarov\\Documents\\Selenium\\senat_admin\\scenarios\\go_to_site.py",
}


def run_scenario():
    # Get the selected radiobutton value
    selected_scenario = scenario.get(var.get())

    # Get the full path of the Python interpreter
    python_executable = os.path.join(sys.prefix, 'python.exe')

    # Run the selected scenario
    cmd = [python_executable, selected_scenario]
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    output, error = process.communicate()

    # Display the output in the textbox
    textbox.configure(state='normal')
    textbox.delete('1.0', tk.END)
    textbox.insert(tk.END, output)
    textbox.configure(state='disabled')


# Create the GUI window
window = tk.Tk()
window.title("E-qaror test")

# Create a custom style for the radiobuttons
style = ttk.Style()
style.configure('TRadiobutton', font=('Segoe UI', 12))

# Create the frame for the radiobuttons and start button
radio_frame = ttk.Frame(window)
radio_frame.pack(side='left', padx=10, pady=10)

# Create the radiobuttons
var = tk.StringVar()
for key in scenario:
    rb = ttk.Radiobutton(radio_frame, text=key, variable=var, value=key)
    rb.pack(pady=5, anchor='w')

# Create a custom style for the start button
style.configure('TButton', font=('Arial', 12), foreground='black', background='#096f35', padding=10, relief='flat', borderwidth=0)
style.map('TButton', background=[('active', '#085e2f')])

# Create the start button
start_button = ttk.Button(window, text="Start", command=run_scenario, style='TButton')
start_button.pack(side='right', padx=10, pady=10)

# Create the textbox
textbox = tk.Text(window, height=10, state='disabled')
textbox.pack(fill='both', expand=True, padx=10, pady=10)

# Set the textbox font
textbox.tag_configure('mono', font=('Consolas', 12))
textbox.insert(tk.END, '', 'mono')

# Set the window background color
window.configure(bg='white')

# Set the button border corners to 10px
window.option_add('*TButton*borderWidth', 0)
window.option_add('*TButton*borderRadius', 10)

# Set the button text color to black
style.configure('TButton', foreground='black')

# Start the GUI main loop
window.mainloop()
