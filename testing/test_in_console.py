import subprocess

# Dictionary of file routes
scenarios = {
    "Go to site": "C:\\Users\\a_gaibnazarov\\Documents\\Selenium\\senat_admin\\scenarios\\go_to_site.py",
    # "Authenticate": "C:\\Users\\a_gaibnazarov\\Documents\\Selenium\\senat_admin\\scenarios\\authentication.py",
    # "Users": "C:\\Users\\a_gaibnazarov\\Documents\\Selenium\\senat_admin\\scenarios\\users.py",
    # "Delete": "C:\\Users\\a_gaibnazarov\\Documents\\Selenium\\senat_admin\\scenarios\\delete_temporary_data.py"
}

# Iterate over the dictionary and run each file in CMD
for file_name, file_route in scenarios.items():
    try:
        # Run the file in CMD
        subprocess.run(["python", file_route], check=True)
        print(f"{file_name} executed successfully.")
    except subprocess.CalledProcessError as e:
        print(f"Error running {file_name}: {e}")
