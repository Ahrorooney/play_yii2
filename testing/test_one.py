from selenium import webdriver
from colorama import Fore, Style
from selenium.webdriver.chrome.options import Options


class BaseMethods:

    def __init__(self, without_window=True) -> None:
        self.driver = None
        # Site's base url, you can change this line when change testing environment
        self.base_url = 'http:/localhost:8080/'
        self.without_window = without_window
        self.setup_method()

    def setup_method(self):
        # This option is for toggle visibility of the browser during test
        self.chrome_options = Options()
        if self.without_window:
            self.chrome_options.add_argument("--headless")

        # Opening browser for testing purpose
        self.driver = webdriver.Chrome('C:\chromedriver\chromedriver.exe', options=self.chrome_options)

        # Maximize browser's view during test
        self.driver.maximize_window()
        # Kind of timeout that waits until element is loaded fully before starting executing next lines
        self.driver.implicitly_wait(0.5)

    def teardown_method(self):
        """Closes browser and exits driver"""
        self.driver.quit()

    def test_go_to_site_index(self):
        """Tests entering to the first page of the Admin Page
        """

        self.driver.get(self.base_url+"site/login")

        self.driver.implicitly_wait(0.5)
        # check if title is same as the first page of the site
        title = self.driver.title
        assert title == "Login"
        print(Fore.GREEN + "test_site_index passed successfully!" + Style.RESET_ALL)

base = BaseMethods()

base.test_go_to_site_index()
base.driver.implicitly_wait(0.5)
base.teardown_method()
