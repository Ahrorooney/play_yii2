<?php

namespace common\helpers;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class BaseController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'], // Restrict access to non-authenticated users
                        'denyCallback' => function ($rule, $action) {
                            return $this->redirect(['site/login']); // Redirect to the login page
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'delete', 'update', 'view-full'],
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

}