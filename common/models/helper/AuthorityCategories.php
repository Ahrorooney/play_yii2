<?php

namespace common\models\helper;

/**
 * This is the model class for table "helper.authority_categories".
 *
 * @property int $id
 * @property int|null $origin_id
 * @property string|null $title_uz
 * @property string|null $title_oz
 * @property string|null $title_ru
 * @property int|null $order
 * @property int|null $code_name
 */
class AuthorityCategories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'helper.authority_categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['origin_id', 'order', 'code_name'], 'default', 'value' => null],
            [['origin_id', 'order', 'code_name'], 'integer'],
            [['title_uz', 'title_oz', 'title_ru'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'origin_id' => 'Origin ID',
            'title_uz' => 'Title Uz',
            'title_oz' => 'Title Oz',
            'title_ru' => 'Title Ru',
            'order' => 'Order',
            'code_name' => 'Code Name',
        ];
    }
}
