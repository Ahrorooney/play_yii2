<?php

namespace common\models\helper;

use Yii;

/**
 * This is the model class for table "helper.decree_type".
 *
 * @property int $id
 * @property string $title_oz
 * @property string $title_uz
 * @property string|null $title_qr
 * @property string|null $title_ru
 * @property int $sort
 * @property bool $is_active
 * @property string|null $created_at
 * @property int|null $creator_id
 * @property string|null $updated_at
 * @property int|null $modifier_id
 *
 * @property Decree[] $decrees
 */
class DecreeType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'helper.decree_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_oz', 'title_uz'], 'required'],
            [['sort', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['sort', 'creator_id', 'modifier_id'], 'integer'],
            [['is_active'], 'boolean'],
            [['created_at', 'updated_at'], 'safe'],
            [['title_oz', 'title_uz', 'title_qr', 'title_ru'], 'string', 'max' => 255],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => UsersUsers::class, 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => UsersUsers::class, 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_oz' => 'Title Oz',
            'title_uz' => 'Title Uz',
            'title_qr' => 'Title Qr',
            'title_ru' => 'Title Ru',
            'sort' => 'Sort',
            'is_active' => 'Is Active',
            'created_at' => 'Created At',
            'creator_id' => 'Creator ID',
            'updated_at' => 'Updated At',
            'modifier_id' => 'Modifier ID',
        ];
    }

    /**
     * Gets query for [[Decrees]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDecrees()
    {
        return $this->hasMany(Decree::class, ['decree_type_id' => 'id']);
    }
}
