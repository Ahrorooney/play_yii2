<?php

namespace common\models\helper;

use common\models\users\User;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "helper.translate".
 *
 * @property int $id
 * @property string $keyword
 * @property string|null $title_uz
 * @property string|null $title_oz
 * @property string|null $title_qr
 * @property string|null $title_ru
 * @property string|null $created_at
 * @property int|null $creator_id
 * @property string|null $updated_at
 * @property int|null $modifier_id
 *
 * @property User $creator
 * @property \common\models\users\User $modifier
 */
class Translate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'helper.translate';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'creator_id',
                'updatedByAttribute' => 'modifier_id',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['keyword'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['creator_id', 'modifier_id'], 'default', 'value' => null],
            [['creator_id', 'modifier_id'], 'integer'],
            [['keyword', 'title_uz', 'title_oz', 'title_qr', 'title_ru'], 'string', 'max' => 255],
            [['keyword'], 'unique'],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keyword' => 'Keyword',
            'title_uz' => 'Title Uz',
            'title_oz' => 'Title Oz',
            'title_qr' => 'Title Qr',
            'title_ru' => 'Title Ru',
            'created_at' => 'Created At',
            'creator_id' => 'Creator ID',
            'updated_at' => 'Updated At',
            'modifier_id' => 'Modifier ID',
        ];
    }

    public function getCreator()
    {
        return $this->hasOne(User::class, ['id' => 'creator_id']);
    }

    public function getModifier()
    {
        return $this->hasOne(User::class, ['id' => 'modifier_id']);
    }
}
