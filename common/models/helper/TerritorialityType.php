<?php

namespace common\models\helper;

/**
 * This is the model class for table "helper.territoriality_type".
 *
 * @property int $id
 * @property int|null $origin_id
 * @property string|null $title_oz
 * @property string|null $title_uz
 * @property string|null $title_ru
 */
class TerritorialityType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'helper.territoriality_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['origin_id'], 'default', 'value' => null],
            [['origin_id'], 'integer'],
            [['title_oz', 'title_uz', 'title_ru'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'origin_id' => 'Origin ID',
            'title_oz' => 'Title Oz',
            'title_uz' => 'Title Uz',
            'title_ru' => 'Title Ru',
        ];
    }
}
