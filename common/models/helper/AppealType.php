<?php

namespace common\models\helper;

use Yii;

/**
 * This is the model class for table "helper.appeal_type".
 *
 * @property int $id
 * @property string $title_oz
 * @property string $title_uz
 * @property string|null $title_qr
 * @property string|null $title_ru
 * @property string|null $created_at
 * @property int|null $creator_id
 * @property string|null $updated_at
 * @property int|null $modifier_id
 *
 * @property Appeal[] $appeals
 */
class AppealType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'helper.appeal_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_oz', 'title_uz'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['creator_id', 'modifier_id'], 'default', 'value' => null],
            [['creator_id', 'modifier_id'], 'integer'],
            [['title_oz', 'title_uz', 'title_qr', 'title_ru'], 'string', 'max' => 255],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => UsersUsers::class, 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => UsersUsers::class, 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_oz' => 'Title Oz',
            'title_uz' => 'Title Uz',
            'title_qr' => 'Title Qr',
            'title_ru' => 'Title Ru',
            'created_at' => 'Created At',
            'creator_id' => 'Creator ID',
            'updated_at' => 'Updated At',
            'modifier_id' => 'Modifier ID',
        ];
    }

    /**
     * Gets query for [[Appeals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAppeals()
    {
        return $this->hasMany(Appeal::class, ['type_id' => 'id']);
    }
}
