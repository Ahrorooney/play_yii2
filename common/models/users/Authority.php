<?php

namespace common\models\users;

use common\helpers\ConstantsHelper;
use common\models\Document;
use common\models\helper\AuthorityCategories;
use common\models\helper\TerritorialityType;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "users.authority".
 *
 * @property int $id
 * @property string $title_oz
 * @property string $title_uz
 * @property string|null $title_qr
 * @property string|null $title_ru
 * @property int $type_id
 * @property int|null $parent_id
 * @property string $code
 * @property string|null $created_at
 * @property int|null $creator_id
 * @property string|null $updated_at
 * @property int|null $modifier_id
 * @property int|null $origin_id
 * @property int|null $main_parent_id
 * @property int|null $territoriality_type_id
 * @property int|null $category_id
 * @property int|null $region_id
 * @property int|null $district_id
 * @property int|null $belongs_region_id
 * @property int|null $belongs_district_id
 * @property string|null $code_name
 * @property string|null $inn
 * @property string|null $is_juridical
 * @property int|null $level
 *
 * @property Authority $parent
 * @property AuthorityType $authorityType
 * @property Authority $mainParent
 * @property \common\models\helper\TerritorialityType $territorialityType
 * @property AuthorityCategories $authorityCategories
 * @property Regions $regions
 * @property Regions $district
 * @property Regions $belongsRegion
 * @property Regions $belongsDistrict
 * @property Document[] $documents
 */
class Authority extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users.authority';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'creator_id',
                'updatedByAttribute' => 'modifier_id',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_oz', 'title_uz', 'type_id', 'code'], 'required'],
            [['type_id', 'parent_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['type_id', 'parent_id', 'creator_id', 'modifier_id', 'origin_id', 'main_parent_id',
                'territoriality_type_id', 'category_id', 'region_id', 'district_id', 'belongs_region_id',
                'belongs_district_id', 'level'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title_oz', 'title_uz', 'title_qr', 'title_ru'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 32],
            [['code_name'], 'string', 'max' => 100],
            [['is_juridical'], 'boolean'],
            [['inn'], 'string', 'max' => 20],
            [['code'], 'unique'],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Authority::class, 'targetAttribute' => ['parent_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => AuthorityType::class, 'targetAttribute' => ['type_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_oz' => 'Title Oz',
            'title_uz' => 'Title Uz',
            'title_qr' => 'Title Qr',
            'title_ru' => 'Title Ru',
            'type_id' => 'Type ID',
            'parent_id' => 'Parent ID',
            'code' => 'Code',
            'created_at' => 'Created At',
            'creator_id' => 'Creator ID',
            'updated_at' => 'Updated At',
            'modifier_id' => 'Modifier ID',
        ];
    }

    /**
     * Gets query for [[Documents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Document::class, ['authority_id' => 'id']);
    }

    public function getParent(): \yii\db\ActiveQuery
    {
        return $this->hasOne(Authority::class, ['id' => 'parent_id']);
    }

    public function getAuthorityType()
    {
        return $this->hasOne(AuthorityType::class, ['id' => 'type_id']);
    }

    /**
     * @return array
     */
    public function getAuthoritiesSelect()
    {
        $out = [];
        foreach (Authority::find()->select('id, title_oz')->asArray()->all() as $authority)
            $out[$authority['id']] = $authority['title_oz'];
        return $out;
    }

    /**
     * @return array
     */
    public function getAuthorityTypesSelect()
    {
        $out = [];
        foreach (AuthorityType::find()->select('id, title_oz')->asArray()->all() as $authority)
            $out[$authority['id']] = $authority['title_oz'];
        return $out;
    }

    public function getMainParent()
    {
        return $this->hasOne(Authority::class, ['id' => 'main_parent_id']);
    }
    public function getTerritorialityType()
    {
        return $this->hasOne(TerritorialityType::class, ['id' => 'territoriality_type_id']);
    }
    public function getAuthorityCategories()
    {
        return $this->hasOne(AuthorityCategories::class, ['id' => 'category_id']);
    }
    public function getRegions()
    {
        return $this->hasOne(Regions::class, ['id' => 'region_id']);
    }
    public function getDistrict()
    {
        return $this->hasOne(Regions::class, ['id' => 'district_id']);
    }
    public function getBelongsRegion()
    {
        return $this->hasOne(Regions::class, ['id' => 'belongs_region_id']);
    }
    public function getBelongsDistrict()
    {
        return $this->hasOne(Regions::class, ['id' => 'belongs_district_id']);
    }

}
