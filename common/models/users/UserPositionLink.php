<?php

namespace common\models\users;

use app\models\UsersPosition;
use app\models\UsersUsers;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "users.user_position_link".
 *
 * @property int $id
 * @property int $user_id
 * @property int $position_id
 * @property string|null $created_at
 * @property int|null $creator_id
 * @property string|null $updated_at
 * @property int|null $modifier_id
 *
 * @property User $user
 * @property Position $position
 */
class UserPositionLink extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users.user_position_link';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'creator_id',
                'updatedByAttribute' => 'modifier_id',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'position_id'], 'required'],
            [['user_id', 'position_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['user_id', 'position_id', 'creator_id', 'modifier_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['position_id'], 'exist', 'skipOnError' => true, 'targetClass' => Position::class, 'targetAttribute' => ['position_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'position_id' => 'Position ID',
            'created_at' => 'Created At',
            'creator_id' => 'Creator ID',
            'updated_at' => 'Updated At',
            'modifier_id' => 'Modifier ID',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getPosition()
    {
        return $this->hasOne(Position::class, ['id' => 'position_id']);
    }

    public function getPositionSelect()
    {
        $out = [];
        foreach (Position::find()->select('id, title_oz')->asArray()->all() as $position)
            $out[$position['id']] = $position['title_oz'];
        return $out;
    }
}
