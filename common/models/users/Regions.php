<?php

namespace common\models\users;

use common\models\Appeal;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "users.regions".
 *
 * @property int $id
 * @property int|null $parent_id
 * @property string|null $title_oz
 * @property string|null $title_uz
 * @property string|null $title_qr
 * @property string|null $title_ru
 * @property int|null $sort
 * @property int|null $level
 * @property bool|null $is_city
 * @property int|null $soato
 * @property int|null $creator_id
 * @property int|null $created_at
 * @property int|null $modifier_id
 * @property int|null $updated_at
 * @property int|null $origin_id
 * @property string|null $code_lat
 * @property string|null $code_cyr
 * @property string|null $start_date
 * @property string|null $finish_date
 * @property int|null $order
 * @property int|null $area_code
 * @property int|null $country_id
 * @property float|null $population
 * @property string|null $map_id
 *
 * @property Appeal[] $appeals
 * @property Appeal[] $appeals0
 * @property Regions $parent
 * @property User $creator
 * @property User $modifier
 */
class Regions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users.regions';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'creator_id',
                'updatedByAttribute' => 'modifier_id',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'sort', 'level', 'soato', 'creator_id', 'created_at', 'modifier_id', 'updated_at', 'origin_id', 'order', 'area_code', 'country_id'], 'default', 'value' => null],
            [['parent_id', 'sort', 'level', 'soato', 'creator_id', 'created_at', 'modifier_id', 'updated_at', 'origin_id', 'order', 'area_code', 'country_id'], 'integer'],
            [['is_city'], 'boolean'],
            [['population'], 'number'],
            [['title_oz', 'title_uz', 'title_qr', 'title_ru', 'start_date', 'finish_date', 'map_id'], 'string', 'max' => 255],
            [['code_lat', 'code_cyr'], 'string', 'max' => 5],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Regions::class, 'targetAttribute' => ['parent_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'title_oz' => 'Title Oz',
            'title_uz' => 'Title Uz',
            'title_qr' => 'Title Qr',
            'title_ru' => 'Title Ru',
            'sort' => 'Sort',
            'level' => 'Level',
            'is_city' => 'Is City',
            'soato' => 'Soato',
            'creator_id' => 'Creator ID',
            'created_at' => 'Created At',
            'modifier_id' => 'Modifier ID',
            'updated_at' => 'Updated At',
            'origin_id' => 'Origin ID',
            'code_lat' => 'Code Lat',
            'code_cyr' => 'Code Cyr',
            'start_date' => 'Start Date',
            'finish_date' => 'Finish Date',
            'order' => 'Order',
            'area_code' => 'Area Code',
            'country_id' => 'Country ID',
            'population' => 'Population',
            'map_id' => 'Map ID',
        ];
    }

    /**
     * Gets query for [[Appeals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAppeals()
    {
        return $this->hasMany(Appeal::class, ['region_id' => 'id']);
    }

    /**
     * Gets query for [[Appeals0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAppeals0()
    {
        return $this->hasMany(Appeal::class, ['district_id' => 'id']);
    }

    public function getParent()
    {
        return $this->hasOne(Regions::class, ['id' => 'parent_id']);
    }

    public function getCreator()
    {
        return $this->hasOne(User::class, ['id' => 'creator_id']);
    }

    public function getModifier()
    {
        return $this->hasOne(User::class,['id' => 'modifier_id']);
    }

    public function getParentsSelect()
    {
        $out = [];
        foreach (Regions::find()->select('id, title_uz')->asArray()->all() as $region)
            $out[$region['id']] = $region['title_uz'];
        return $out;
    }
}
