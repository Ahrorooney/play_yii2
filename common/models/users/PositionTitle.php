<?php

namespace common\models\users;

use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "users.position_title".
 *
 * @property int $id
 * @property int $user_id
 * @property string $title_oz
 * @property string $title_uz
 * @property string|null $title_qr
 * @property string|null $title_ru
 * @property string|null $created_at
 * @property int|null $creator_id
 * @property string|null $updated_at
 * @property int|null $modifier_id
 *
 * @property User $user
 */
class PositionTitle extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users.position_title';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'creator_id',
                'updatedByAttribute' => 'modifier_id',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'title_oz', 'title_uz'], 'required'],
            [['user_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['user_id', 'creator_id', 'modifier_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title_oz', 'title_uz', 'title_qr', 'title_ru'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'title_oz' => 'Title Oz',
            'title_uz' => 'Title Uz',
            'title_qr' => 'Title Qr',
            'title_ru' => 'Title Ru',
            'created_at' => 'Created At',
            'creator_id' => 'Creator ID',
            'updated_at' => 'Updated At',
            'modifier_id' => 'Modifier ID',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getUserSelect()
    {
        $out = [];
        foreach (User::find()->select("id, first_name, surname")->asArray()->all() as $user)
            $out[$user['id']] = $user['first_name']. ' '. $user['surname'];
        return $out;
    }
}
