<?php

namespace common\models\users;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users.users".
 *
 * @property int $id
 * @property string $pin
 * @property string $username
 * @property string $first_name
 * @property string|null $surname
 * @property string|null $middle_name Sharifi, отчество
 * @property string|null $image_path
 * @property string $password_hash
 * @property string|null $created_at
 * @property int|null $creator_id
 * @property string|null $updated_at
 * @property int|null $modifier_id
 *
// * @property Appeal[] $appeals
// * @property Appeal[] $appeals0
// * @property Appeal[] $appeals1
// * @property DocumentAnswerLink[] $documentAnswerLinks
// * @property DocumentAnswerLink[] $documentAnswerLinks0
// * @property DocumentRelatedLink[] $documentRelatedLinks
// * @property DocumentRelatedLink[] $documentRelatedLinks0
// * @property DocumentType[] $documentTypes
// * @property DocumentType[] $documentTypes0
// * @property Document[] $documents
// * @property Document[] $documents0
// * @property FileType[] $fileTypes
// * @property FileType[] $fileTypes0
// * @property File[] $files
// * @property File[] $files0
// * @property TaskDocumentLink[] $taskDocumentLinks
// * @property TaskDocumentLink[] $taskDocumentLinks0
// * @property Task[] $tasks
// * @property Task[] $tasks0
// * @property Task[] $tasks1
// * @property Task[] $tasks2
 * @property string $fullName
 * @property string $password write-only password
 * @property string $rolesAsBadges
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users.users';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'creator_id',
                'updatedByAttribute' => 'modifier_id',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pin', 'first_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['creator_id', 'modifier_id'], 'default', 'value' => null],
            [['creator_id', 'modifier_id'], 'integer'],
            [['pin'], 'string', 'max' => 32],
            [['first_name', 'surname', 'middle_name', 'image_path', 'password_hash', 'username'], 'string', 'max' => 255],
            [['pin'], 'unique'],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pin' => 'Pin',
            'first_name' => 'First Name',
            'surname' => 'Surname',
            'middle_name' => 'Middle Name',
            'image_path' => 'Image Path',
            'created_at' => 'Created At',
            'creator_id' => 'Creator ID',
            'updated_at' => 'Updated At',
            'modifier_id' => 'Modifier ID',
        ];
    }

//    /**
//     * Gets query for [[Appeals]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getAppeals()
//    {
//        return $this->hasMany(Appeal::class, ['senator_id' => 'id']);
//    }
//
//    /**
//     * Gets query for [[Appeals0]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getAppeals0()
//    {
//        return $this->hasMany(Appeal::class, ['creator_id' => 'id']);
//    }
//
//    /**
//     * Gets query for [[Appeals1]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getAppeals1()
//    {
//        return $this->hasMany(Appeal::class, ['modifier_id' => 'id']);
//    }
//
//    /**
//     * Gets query for [[DocumentAnswerLinks]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getDocumentAnswerLinks()
//    {
//        return $this->hasMany(DocumentAnswerLink::class, ['creator_id' => 'id']);
//    }
//
//    /**
//     * Gets query for [[DocumentAnswerLinks0]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getDocumentAnswerLinks0()
//    {
//        return $this->hasMany(DocumentAnswerLink::class, ['modifier_id' => 'id']);
//    }
//
//    /**
//     * Gets query for [[DocumentRelatedLinks]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getDocumentRelatedLinks()
//    {
//        return $this->hasMany(DocumentRelatedLink::class, ['creator_id' => 'id']);
//    }
//
//    /**
//     * Gets query for [[DocumentRelatedLinks0]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getDocumentRelatedLinks0()
//    {
//        return $this->hasMany(DocumentRelatedLink::class, ['modifier_id' => 'id']);
//    }
//
//    /**
//     * Gets query for [[DocumentTypes]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getDocumentTypes()
//    {
//        return $this->hasMany(DocumentType::class, ['creator_id' => 'id']);
//    }
//
//    /**
//     * Gets query for [[DocumentTypes0]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getDocumentTypes0()
//    {
//        return $this->hasMany(DocumentType::class, ['modifier_id' => 'id']);
//    }
//
//    /**
//     * Gets query for [[Documents]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getDocuments()
//    {
//        return $this->hasMany(Document::class, ['creator_id' => 'id']);
//    }
//
//    /**
//     * Gets query for [[Documents0]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getDocuments0()
//    {
//        return $this->hasMany(Document::class, ['modifier_id' => 'id']);
//    }
//
//    /**
//     * Gets query for [[FileTypes]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getFileTypes()
//    {
//        return $this->hasMany(FileType::class, ['creator_id' => 'id']);
//    }
//
//    /**
//     * Gets query for [[FileTypes0]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getFileTypes0()
//    {
//        return $this->hasMany(FileType::class, ['modifier_id' => 'id']);
//    }
//
//    /**
//     * Gets query for [[Files]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getFiles()
//    {
//        return $this->hasMany(File::class, ['creator_id' => 'id']);
//    }
//
//    /**
//     * Gets query for [[Files0]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getFiles0()
//    {
//        return $this->hasMany(File::class, ['modifier_id' => 'id']);
//    }
//
//    /**
//     * Gets query for [[TaskDocumentLinks]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getTaskDocumentLinks()
//    {
//        return $this->hasMany(TaskDocumentLink::class, ['creator_id' => 'id']);
//    }
//
//    /**
//     * Gets query for [[TaskDocumentLinks0]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getTaskDocumentLinks0()
//    {
//        return $this->hasMany(TaskDocumentLink::class, ['modifier_id' => 'id']);
//    }
//
//    /**
//     * Gets query for [[Tasks]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getTasks()
//    {
//        return $this->hasMany(Task::class, ['user_id' => 'id']);
//    }
//
//    /**
//     * Gets query for [[Tasks0]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getTasks0()
//    {
//        return $this->hasMany(Task::class, ['sender_id' => 'id']);
//    }
//
//    /**
//     * Gets query for [[Tasks1]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getTasks1()
//    {
//        return $this->hasMany(Task::class, ['creator_id' => 'id']);
//    }
//
//    /**
//     * Gets query for [[Tasks2]].
//     *
//     * @return \yii\db\ActiveQuery
//     */
//    public function getTasks2()
//    {
//        return $this->hasMany(Task::class, ['modifier_id' => 'id']);
//    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function getFullName()
    {
        return $this->surname." ".$this->first_name." ".$this->middle_name;
    }
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
//        return $this->auth_key;
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
//        return $this->getAuthKey() === $authKey;
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public function getRoles()
    {
        return UserRoleLink::find()->select('role_id')->andWhere(['user_id' => $this->id])->asArray()->all();
    }

    public function getDepartments()
    {
        $r = UserRoleLink::find()->select('department_id')->andWhere(['user_id' => $this->id])
            ->andWhere(['IS NOT', 'department_id', null])->groupBy('department_id')->asArray()->all();
        return $r;
    }

    public function getRolesAsBadges() {
        $roles = $this->getRoles();
        $string = '';
        foreach ($roles as $role) {
            $role_obj = Role::findOne($role['role_id']);
            $string .= '<span class="badge badge-pill badge-info">'.$role_obj->title_uz.'</span>';
        }
        return $string;
    }
    public function getDepartmentsAsBadges()
    {
        $departments = $this->getDepartments();
        $string = '';
        foreach ($departments as $department) {
            $department_obj = Department::findOne($department['department_id']);
            $string .= '<span class="badge badge-pill badge-info">'.$department_obj->title_uz.'</span>';
        }
        return $string;
    }

//    public static function find()
//    {
//        $query = parent::find();
//        $query->select(['*', 'CONCAT(surname, " ", first_name, " ", middle_name) AS full_name']);
//        $query->orderBy(['full_name_sort' => SORT_ASC]);
//        return $query;
//    }
}
