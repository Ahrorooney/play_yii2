<?php

namespace common\models\users;

use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "users.user_role_link".
 *
 * @property int $id
 * @property int $user_id
 * @property int $role_id
 * @property int|null $department_id
 * @property string|null $created_at
 * @property int|null $creator_id
 * @property string|null $updated_at
 * @property int|null $modifier_id
 *
 * @property User $user
 * @property Role $role
 * @property Department $department
 */
class UserRoleLink extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users.user_role_link';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'creator_id',
                'updatedByAttribute' => 'modifier_id',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'role_id'], 'required'],
            [['user_id', 'role_id', 'department_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['user_id', 'role_id', 'department_id', 'creator_id', 'modifier_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_id', 'role_id', 'department_id'], 'unique', 'targetAttribute' => ['user_id', 'role_id', 'department_id']],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Role::class, 'targetAttribute' => ['role_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'role_id' => 'Role ID',
            'department_id' => 'Department ID',
            'created_at' => 'Created At',
            'creator_id' => 'Creator ID',
            'updated_at' => 'Updated At',
            'modifier_id' => 'Modifier ID',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getRole()
    {
        return $this->hasOne(Role::class, ['id' => 'role_id']);
    }

    public function getDepartment()
    {
        return $this->hasOne(Department::class, ['id' => 'department_id']);
    }

    /**
     * @return array
     */
    public function getUsersSelect()
    {
        $out = [];
        foreach (User::find()->select('id, first_name, surname, middle_name')->asArray()->all() as $user)
            $out[$user['id']] = $user['surname'].' '.$user['first_name'].' '.$user['middle_name'];
        return $out;
    }

    /**
     * @return array
     */
    public function getRolesSelect()
    {
        $out = [];
        foreach (Role::find()->select('id, title_oz')->asArray()->all() as $role)
            $out[$role['id']] = $role['title_oz'];
        return $out;
    }

    /**
     * @return array
     */
    public function getDepartmentsSelect()
    {
        $out = [];
        foreach (Department::find()->select('id, title_oz')->asArray()->all() as $department)
            $out[$department['id']] = $department['title_oz'];
        return $out;
    }

}
