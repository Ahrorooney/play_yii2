<?php

namespace common\models\users;

use common\models\Document;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "users.department".
 *
 * @property int $id
 * @property int $type_id
 * @property string $title_oz
 * @property string $title_uz
 * @property string|null $title_qr
 * @property string|null $title_ru
 * @property int|null $parent_id
 * @property string|null $created_at
 * @property int|null $creator_id
 * @property string|null $updated_at
 * @property int|null $modifier_id
 *
 * @property Department $parent
 * @property DepartmentType $departmentType
 * @property Document[] $documents
 */
class Department extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users.department';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'creator_id',
                'updatedByAttribute' => 'modifier_id',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_id', 'title_oz', 'title_uz'], 'required'],
            [['type_id', 'parent_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['type_id', 'parent_id', 'creator_id', 'modifier_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title_oz', 'title_uz', 'title_qr', 'title_ru'], 'string', 'max' => 255],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::class, 'targetAttribute' => ['parent_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DepartmentType::class, 'targetAttribute' => ['type_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => 'Type ID',
            'title_oz' => 'Title Oz',
            'title_uz' => 'Title Uz',
            'title_qr' => 'Title Qr',
            'title_ru' => 'Title Ru',
            'parent_id' => 'Parent ID',
            'created_at' => 'Created At',
            'creator_id' => 'Creator ID',
            'updated_at' => 'Updated At',
            'modifier_id' => 'Modifier ID',
        ];
    }

    public function getParent(): \yii\db\ActiveQuery
    {
        return $this->hasOne(Department::class, ['id' => 'parent_id']);
    }

    public function getDepartmentType()
    {
        return $this->hasOne(DepartmentType::class, ['id' => 'type_id']);
    }

    /**
     * Gets query for [[Documents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Document::class, ['department_id' => 'id']);
    }

    /**
     * @return array
     */
    public function getDepartmentsSelect()
    {
        $out = [];
        foreach (Department::find()->select('id, title_oz')->asArray()->all() as $department)
            $out[$department['id']] = $department['title_oz'];
        return $out;
    }

    /**
     * @return array
     */
    public function getDepartmentTypesSelect()
    {
        $out = [];
        foreach (DepartmentType::find()->select('id, title_oz')->asArray()->all() as $department)
            $out[$department['id']] = $department['title_oz'];
        return $out;
    }
}
