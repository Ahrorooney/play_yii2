<?php

namespace common\models;

use common\models\users\User;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**S
 * This is the model class for table "public.document_type".
 *
 * @property int $id
 * @property int|null $parent_id
 * @property string $title_oz
 * @property string $title_uz
 * @property string|null $title_qr
 * @property string|null $title_ru
 * @property string|null $created_at
 * @property int|null $creator_id
 * @property string|null $updated_at
 * @property int|null $modifier_id
 *
 * @property DocumentType $parent
 */
class DocumentType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'public.document_type';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => function() {
                    return date('Y-m-d H:i:s');
                }
            ],
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'creator_id',
                'updatedByAttribute' => 'modifier_id',
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['parent_id', 'creator_id', 'modifier_id'], 'integer'],
            [['title_oz', 'title_uz'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['title_oz', 'title_uz', 'title_qr', 'title_ru'], 'string', 'max' => 255],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentType::class, 'targetAttribute' => ['parent_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'title_oz' => 'Title Oz',
            'title_uz' => 'Title Uz',
            'title_qr' => 'Title Qr',
            'title_ru' => 'Title Ru',
            'created_at' => 'Created At',
            'creator_id' => 'Creator ID',
            'updated_at' => 'Updated At',
            'modifier_id' => 'Modifier ID',
        ];
    }

    public function getParent(): \yii\db\ActiveQuery
    {
        return $this->hasOne(DocumentType::class, ['id' => 'parent_id']);
    }

    /**
     * @return array
     */
    public function getDocumentTypesSelect()
    {
        $out = [];
        foreach (DocumentType::find()->select('id, title_oz')->asArray()->all() as $document)
            $out[$document['id']] = $document['title_oz'];
        return $out;
    }
}
