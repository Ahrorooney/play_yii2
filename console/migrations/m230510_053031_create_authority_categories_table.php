<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%authority_categories}}`.
 */
class m230510_053031_create_authority_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%helper.authority_categories}}', [
            'id' => $this->primaryKey(),
            'origin_id' => $this->integer(),
            'title_uz' => $this->string(255),
            'title_oz' => $this->string(255),
            'title_ru' => $this->string(255),
            'order' => $this->integer(),
            'code_name' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%helper.authority_categories}}');
    }
}
