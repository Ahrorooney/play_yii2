<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%users.users}}`.
 */
class m230515_101136_add_username_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%users.users}}', 'username', $this->string(255)->notNull()->defaultValue('username'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%users.users}}', 'username');
    }
}
