<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%authority_type}}`.
 */
class m230510_050723_add_id_origin_column_to_authority_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%users.authority_type}}', 'origin_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%users.authority_type}}', 'origin_id');
    }
}
