<?php

use yii\db\Migration;

/**
 * Class m230510_064415_add_fields_to_regions_table
 */
class m230510_064415_add_fields_to_authority_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%users.authority}}', 'origin_id', $this->integer());
        $this->addColumn('{{%users.authority}}', 'main_parent_id', $this->integer());
        $this->addColumn('{{%users.authority}}', 'territoriality_type_id', $this->integer());
        $this->addColumn('{{%users.authority}}', 'category_id', $this->integer());
        $this->addColumn('{{%users.authority}}', 'region_id', $this->integer());
        $this->addColumn('{{%users.authority}}', 'district_id', $this->integer());
        $this->addColumn('{{%users.authority}}', 'belongs_region_id', $this->integer());
        $this->addColumn('{{%users.authority}}', 'belongs_district_id', $this->integer());
        $this->addColumn('{{%users.authority}}', 'code_name', $this->string(100));
        $this->addColumn('{{%users.authority}}', 'inn', $this->string(20));
        $this->addColumn('{{%users.authority}}', 'is_juridical', $this->boolean());
        $this->addColumn('{{%users.authority}}', 'level', $this->string(50));

        // creates index for column `main_parent_id`
        $this->createIndex(
            'idx-authority-main_parent_id',
            'users.authority',
            'main_parent_id'
        );

        // add foreign key for table `users.authority`
        $this->addForeignKey(
            'fk-authority-main_parent_id',
            'users.authority',
            'main_parent_id',
            'users.authority',
            'id',
            'RESTRICT',
            'RESTRICT'
        );

        // creates index for column `territoriality_type_id`
        $this->createIndex(
            'idx-authority-territoriality_type_id',
            'users.authority',
            'territoriality_type_id'
        );

        // add foreign key for table `helper.territoriality_type`
        $this->addForeignKey(
            'fk-authority-territoriality_type_id',
            'users.authority',
            'territoriality_type_id',
            'helper.territoriality_type',
            'id',
            'RESTRICT',
            'RESTRICT'
        );

        // creates index for column `category_id`
        $this->createIndex(
            'idx-authority-category_id',
            'users.authority',
            'category_id'
        );

        // add foreign key for table `helper.authority_categories`
        $this->addForeignKey(
            'fk-authority-category_id',
            'users.authority',
            'category_id',
            'helper.authority_categories',
            'id',
            'RESTRICT',
            'RESTRICT'
        );

        // creates index for column `region_id`
        $this->createIndex(
            'idx-authority-region_id',
            'users.authority',
            'region_id'
        );

        // add foreign key for table `users.regions`
        $this->addForeignKey(
            'fk-authority-region_id',
            'users.authority',
            'region_id',
            'users.regions',
            'id',
            'RESTRICT',
            'RESTRICT'
        );

        // creates index for column `district_id`
        $this->createIndex(
            'idx-authority-district_id',
            'users.authority',
            'district_id'
        );

        // add foreign key for table `users.regions`
        $this->addForeignKey(
            'fk-authority-district_id',
            'users.authority',
            'district_id',
            'users.regions',
            'id',
            'RESTRICT',
            'RESTRICT'
        );

        // creates index for column `belongs_region_id`
        $this->createIndex(
            'idx-authority-belongs_region_id',
            'users.authority',
            'belongs_region_id'
        );

        // add foreign key for table `users.regions`
        $this->addForeignKey(
            'fk-authority-belongs_region_id',
            'users.authority',
            'belongs_region_id',
            'users.regions',
            'id',
            'RESTRICT',
            'RESTRICT'
        );

        // creates index for column `belongs_district_id`
        $this->createIndex(
            'idx-authority-belongs_district_id',
            'users.authority',
            'belongs_district_id'
        );

        // add foreign key for table `users.regions`
        $this->addForeignKey(
            'fk-authority-belongs_district_id',
            'users.authority',
            'belongs_district_id',
            'users.regions',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m230510_064415_add_fields_to_regions_table cannot be reverted.\n";
        $this->dropColumn('{{%users.authority}}', 'origin_id');
        $this->dropColumn('{{%users.authority}}', 'main_parent_id');
        $this->dropColumn('{{%users.authority}}', 'territoriality_type_id');
        $this->dropColumn('{{%users.authority}}', 'category_id');
        $this->dropColumn('{{%users.authority}}', 'region_id');
        $this->dropColumn('{{%users.authority}}', 'district_id');
        $this->dropColumn('{{%users.authority}}', 'belongs_region_id');
        $this->dropColumn('{{%users.authority}}', 'belongs_district_id');
        $this->dropColumn('{{%users.authority}}', 'code_name');
        $this->dropColumn('{{%users.authority}}', 'inn');
        $this->dropColumn('{{%users.authority}}', 'is_juridical');
        $this->dropColumn('{{%users.authority}}', 'level');

        // drops foreign key for table `users.authority`
        $this->dropForeignKey(
            'fk-authority-main_parent_id',
            'users.authority'
        );

        // drops index for column `main_parent_id`
        $this->dropIndex(
            'idx-authority-main_parent_id',
            'users.authority'
        );

        // drops foreign key for table `helper.territoriality_type`
        $this->dropForeignKey(
            'fk-authority-territoriality_type_id',
            'users.authority'
        );

        // drops index for column `territoriality_type_id`
        $this->dropIndex(
            'idx-authority-territoriality_type_id',
            'users.authority'
        );

        // drops foreign key for table `helper.authority_categories`
        $this->dropForeignKey(
            'fk-authority-category_id',
            'users.authority'
        );

        // drops index for column `category_id`
        $this->dropIndex(
            'idx-authority-category_id',
            'users.authority'
        );

        // drops foreign key for table `users.regions`
        $this->dropForeignKey(
            'fk-authority-region_id',
            'users.authority'
        );

        // drops index for column `region_id`
        $this->dropIndex(
            'idx-authority-region_id',
            'users.authority'
        );

        // drops foreign key for table `users.regions`
        $this->dropForeignKey(
            'fk-authority-district_id',
            'users.authority'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-authority-district_id',
            'users.authority'
        );

        // drops foreign key for table `users.regions`
        $this->dropForeignKey(
            'fk-authority-belongs_region_id',
            'users.authority'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-authority-belongs_region_id',
            'users.authority'
        );

        // drops foreign key for table `users.regions`
        $this->dropForeignKey(
            'fk-authority-belongs_district_id',
            'users.authority'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-authority-belongs_district_id',
            'users.authority'
        );
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230510_064415_add_fields_to_regions_table cannot be reverted.\n";

        return false;
    }
    */
}
