<?php

use yii\db\Migration;

/**
 * Class m230510_055340_add_fields_regions_table
 */
class m230510_055340_add_fields_regions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%users.regions}}', 'origin_id', $this->integer());
        $this->addColumn('{{%users.regions}}', 'code_lat', $this->string(5));
        $this->addColumn('{{%users.regions}}', 'code_cyr', $this->string(5));
        $this->addColumn('{{%users.regions}}', 'start_date', $this->string(255));
        $this->addColumn('{{%users.regions}}', 'finish_date', $this->string(255));
        $this->addColumn('{{%users.regions}}', 'order', $this->integer());
        $this->addColumn('{{%users.regions}}', 'area_code', $this->integer());
        $this->addColumn('{{%users.regions}}', 'country_id', $this->integer());
        $this->addColumn('{{%users.regions}}', 'population', $this->float());
        $this->addColumn('{{%users.regions}}', 'map_id', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m230510_055340_add_fields_regions_table cannot be reverted.\n";
        $this->dropColumn('{{%users.authority_type}}', 'origin_id');
        $this->dropColumn('{{%users.authority_type}}', 'code_lat');
        $this->dropColumn('{{%users.authority_type}}', 'code_cyr');
        $this->dropColumn('{{%users.authority_type}}', 'start_date');
        $this->dropColumn('{{%users.authority_type}}', 'finish_date');
        $this->dropColumn('{{%users.authority_type}}', 'order');
        $this->dropColumn('{{%users.authority_type}}', 'area_code');
        $this->dropColumn('{{%users.authority_type}}', 'country_id');
        $this->dropColumn('{{%users.authority_type}}', 'population');
        $this->dropColumn('{{%users.authority_type}}', 'map_id');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230510_055340_add_fields_regions_table cannot be reverted.\n";

        return false;
    }
    */
}
