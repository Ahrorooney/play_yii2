<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%users.users}}`.
 */
class m230515_095732_add_password_hash_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%users.users}}', 'password_hash', $this->string(255)
            ->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%users.users}}', 'password_hash');
    }
}
