<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%territoriality_type}}`.
 */
class m230510_053728_create_territoriality_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%helper.territoriality_type}}', [
            'id' => $this->primaryKey(),
            'origin_id' => $this->integer(),
            'title_oz' => $this->string(255),
            'title_uz' => $this->string(255),
            'title_ru' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%helper.territoriality_type}}');
    }
}
