<?php

use yii\db\Migration;

/**
 * Class m230512_051640_add_fields_to_user_position_link
 */
class m230512_051640_add_fields_to_user_position_link extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%users.user_position_link}}', 'created_at', $this->timestamp()->defaultValue(new \yii\db\Expression('CURRENT_TIMESTAMP')));
        $this->addColumn('{{%users.user_position_link}}', 'creator_id', $this->integer());
        $this->addColumn('{{%users.user_position_link}}', 'updated_at', $this->timestamp());
        $this->addColumn('{{%users.user_position_link}}', 'modifier_id', $this->integer());

        // creates index for column `creator_id`
        $this->createIndex(
            'idx-users_user_position_link-cre',
            'users.user_position_link',
            'creator_id'
        );

        // add foreign key for table `users.users`
        $this->addForeignKey(
            'fk-users_user_position_link-cre',
            'users.user_position_link',
            'creator_id',
            'users.users',
            'id',
            'RESTRICT',
            'RESTRICT'
        );

        // creates index for column `modifier_id`
        $this->createIndex(
            'idx-users_user_position_link-mod',
            'users.user_position_link',
            'modifier_id'
        );

        // add foreign key for table `users.users`
        $this->addForeignKey(
            'fk-users_user_position_link-mod',
            'users.user_position_link',
            'modifier_id',
            'users.users',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m230512_051640_add_fields_to_user_position_link cannot be reverted.\n";
        $this->dropColumn('{{%users.user_position_link}}', 'created_at');
        $this->dropColumn('{{%users.user_position_link}}', 'creator_id');
        $this->dropColumn('{{%users.user_position_link}}', 'updated_at');
        $this->dropColumn('{{%users.user_position_link}}', 'modifier_id');

//         drops foreign key for table `users.user_position_link`
        $this->dropForeignKey(
            'fk-users_user_position_link-cre',
            '{{%users.user_position_link}}'
        );

        // drops index for column `creator_id`
        $this->dropIndex(
            'idx-users_user_position_link-cre',
            '{{%users.user_position_link}}'
        );

        // drops foreign key for table `users.user_position_link`
        $this->dropForeignKey(
            'fk-users_user_position_link-mod',
            '{{%users.user_position_link}}'
        );

        // drops index for column `modifier_id`
        $this->dropIndex(
            'idx-users_user_position_link-mod',
            '{{%users.user_position_link}}'
        );
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230512_051640_add_fields_to_user_position_link cannot be reverted.\n";

        return false;
    }
    */
}
