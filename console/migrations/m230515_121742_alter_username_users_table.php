<?php

use yii\db\Migration;

/**
 * Class m230515_121742_alter_username_users_table
 */
class m230515_121742_alter_username_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%users.users}}', 'username', $this->string(255)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m230515_121742_alter_username_users_table cannot be reverted.\n";
        $this->alterColumn('{{%users.users}}', 'username', $this->string(255)->notNull()->defaultValue('username'));
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m230515_121742_alter_username_users_table cannot be reverted.\n";

        return false;
    }
    */
}
