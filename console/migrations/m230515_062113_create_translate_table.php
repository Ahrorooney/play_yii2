<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%helper_translate}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%users.users}}`
 * - `{{%users.users}}`
 */
class m230515_062113_create_translate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%helper.translate}}', [
            'id' => $this->primaryKey(),
            'keyword' => $this->string(255)->notNull()->unique(),
            'title_uz' => $this->string(255),
            'title_oz' => $this->string(255),
            'title_qr' => $this->string(255),
            'title_ru' => $this->string(255),
            'created_at' => $this->timestamp()->defaultValue(new \yii\db\Expression('CURRENT_TIMESTAMP')),
            'creator_id' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'modifier_id' => $this->integer(),
        ]);

        // creates index for column `creator_id`
        $this->createIndex(
            '{{%idx-translate-creator_id}}',
            '{{%helper.translate}}',
            'creator_id'
        );

        // add foreign key for table `{{%users.users}}`
        $this->addForeignKey(
            '{{%fk-translate-creator_id}}',
            '{{%helper.translate}}',
            'creator_id',
            '{{%users.users}}',
            'id',
            'CASCADE'
        );

        // creates index for column `modifier_id`
        $this->createIndex(
            '{{%idx-translate-modifier_id}}',
            '{{%helper.translate}}',
            'modifier_id'
        );

        // add foreign key for table `{{%users.users}}`
        $this->addForeignKey(
            '{{%fk-translate-modifier_id}}',
            '{{%helper.translate}}',
            'modifier_id',
            '{{%users.users}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%users.users}}`
        $this->dropForeignKey(
            '{{%fk-translate-creator_id}}',
            '{{%helper.translate}}'
        );

        // drops index for column `creator_id`
        $this->dropIndex(
            '{{%idx-translate-creator_id}}',
            '{{%helper.translate}}'
        );

        // drops foreign key for table `{{%users.users}}`
        $this->dropForeignKey(
            '{{%fk-translate-modifier_id}}',
            '{{%helper.translate}}'
        );

        // drops index for column `modifier_id`
        $this->dropIndex(
            '{{%idx-translate-modifier_id}}',
            '{{%helper.translate}}'
        );

        $this->dropTable('{{%helper.translate}}');
    }
}
